package resysclagem.ws;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

public class PropertySource {
	public static Properties props = new Properties();
	public static Properties versionProps = new Properties();
	
	static Map<String, Properties> pathMap = new HashMap<>();
	//main properties file
	public static String mainFilePath = "resources" + File.separator + "resysclagem.properties";
	// version properties file
	public static String versionFilePath = "resources" + File.separator + "version.properties";
	
	public static boolean retry = true;
	
	static {
		boolean origRetry = retry;
		pathMap.put(mainFilePath, props);
		pathMap.put(versionFilePath, versionProps);
		
		System.out.println("* Properties files: ");
		
		for (Entry<String, Properties> propEntry : pathMap.entrySet()) {
			String filePath = propEntry.getKey();
			Properties prop = propEntry.getValue();
			File propertiesFile = new File(filePath);
			try {
				prop.load(new FileInputStream(propertiesFile));
				System.out.println("* " + filePath);
			// If properties file is not found, try /src
			} catch (IOException io1) {
				if(retry){
					//retry = false;
					filePath = "src" + File.separator + filePath;	//prod
					try {
						prop.load(new FileInputStream(filePath));
						System.out.println("* Properties file: " + filePath);
					}catch (Exception io2) {
						System.err.println( new File("").getAbsolutePath() );
						io1.printStackTrace();
						io2.printStackTrace();
					}
				}
			}
		}
		
	}
}