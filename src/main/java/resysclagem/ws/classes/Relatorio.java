package resysclagem.ws.classes;

import java.util.List;

public class Relatorio {
	private int codRel; 
	private String nome;
	private String query;
    private String dataInicial;
    private String dataFinal;
    private boolean ignoraDatas;
    private String ordem;			// ASC ou DESC
    private List<?> dadoVertical;	//array
    private List<?> dadoHorizontal;  //array de {data: [28, 48], label: 'Series B'}
	
    public Relatorio(int codRel, String nome, String query, String dataInicial, String dataFinal, boolean ignoraDatas, String ordem, 
    		List<?> dadoVertical, List<?> dadoHorizontal) {
		super();
		this.codRel = codRel;
		this.nome = nome;
		this.query = query;
		this.dataInicial = dataInicial;
		this.dataFinal = dataFinal;
		this.ignoraDatas = ignoraDatas;
		this.ordem = ordem;
		this.dadoVertical = dadoVertical;
		this.dadoHorizontal = dadoHorizontal;
	}

	public int getCodRel() {
		return codRel;
	}

	public void setCodRel(int codRel) {
		this.codRel = codRel;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(String dataInicial) {
		this.dataInicial = dataInicial;
	}

	public String getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(String dataFinal) {
		this.dataFinal = dataFinal;
	}

	public List<?> getDadoVertical() {
		return dadoVertical;
	}

	public void setDadoVertical(List<?> dadoVertical) {
		this.dadoVertical = dadoVertical;
	}

	public List<?> getDadoHorizontal() {
		return dadoHorizontal;
	}

	public void setDadoHorizontal(List<?> dadoHorizontal) {
		this.dadoHorizontal = dadoHorizontal;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
	
	public void fixQuery() {
		if(this.query.contains("\u003d")){
			String fixed = this.query.replaceAll("\u003d","=");
			this.query = fixed;
		}
	}

	public String getOrdem() {
		return ordem;
	}

	public void setOrdem(String ordem) {
		this.ordem = ordem;
	}

	public boolean isIgnoraDatas() {
		return ignoraDatas;
	}

	public void setIgnoraDatas(boolean ignoraDatas) {
		this.ignoraDatas = ignoraDatas;
	}
    
    
}
