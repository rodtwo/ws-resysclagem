package resysclagem.ws.classes;

import java.util.List;

import org.apache.commons.io.FilenameUtils;

public class Residuo {
	private int codR;
	private String nome;
	private String descricao;
	private String imagemResiduo;
	//private String imagemSubcategoria;
	private Categoria categoria;
	private List<Dica> dicas;

	public Residuo(int codR, String nome, String descricao, String imagemResiduo) {
		this(nome, descricao, imagemResiduo);
		this.codR = codR;
	}
	
	public Residuo(String nome, String descricao, String imagemResiduo) {
		super();
		this.nome = nome;
		this.descricao = descricao;
		setImagemResiduo(imagemResiduo);
	}
	
	public int getCodR() {
		return codR;
	}

	public void setCodR(int codR) {
		this.codR = codR;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getImagemResiduo() {
		return imagemResiduo;
	}

	public void setImagemResiduo(String imagemResiduo) {
		this.imagemResiduo = imagemResiduo;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
	public List<Dica> getDicas() {
		return dicas;
	}

	public void setDicas(List<Dica> dicas) {
		this.dicas = dicas;
	}

	@Override
	public boolean equals(Object obj){
		Residuo res = (Residuo) obj;
	  	return this.codR == res.codR;
	}
	
	public void imageNameStandard(FilePath fp) {
		String extension = FilenameUtils.getExtension(this.getImagemResiduo()).toLowerCase();
		if(extension.contains("jpeg")){ 
			extension = extension.replace("jpeg", "jpg");
		}
		String imagePath = fp.getFullPath()
				+ "/"
				+ this.getCodR()
				+ "-big."
				+ extension;
		this.imagemResiduo = imagePath;
	}
	
}
