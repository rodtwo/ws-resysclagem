package resysclagem.ws.classes;

import java.io.File;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.util.Collections;

import jersey.repackaged.com.google.common.collect.Collections2;
import resysclagem.ws.PropertySource;
import resysclagem.ws.launch.JettyLauncher;

public class FilePath {
	private PathType type;
	private String baseFilePath;
	private String appendedPath;

	//enums
	
	public enum PathType {	
		LOCAL("L"),
		SERVICE("S");
		public final String property;
		PathType(String prop){
			this.property = prop;
		}
		
		public String getProp(){
			return this.property;
		}
	}
	
	public enum AppendEnum {	
		DICA(PropertySource.props.getProperty("resysclagem.file.dica")),
		RELATORIO(PropertySource.props.getProperty("resysclagem.file.relatorio")),
		PONTOCOLETA(PropertySource.props.getProperty("resysclagem.file.img.pontocoleta")),
		RESIDUO(PropertySource.props.getProperty("resysclagem.file.img.residuo"));
		public final String property;
		AppendEnum(String prop){
			this.property = prop;
		}
		
		public String getProp(){
			return this.property;
		}
	}
	
	//constructors
	
	/** 
	 * Use this constructor passing true ONLY IF calling it from src/test package
	 */
	public FilePath(PathType type, AppendEnum appendedEnum, boolean isExecFromTest){
		this.construct(type, appendedEnum, isExecFromTest);
	}
	
	/** 
	 * Regular constructor
	 */
	public FilePath(PathType type, AppendEnum appendedEnum){
		this.construct(type, appendedEnum, false);
	}
	
	// "real" constructor
	private void construct(PathType type, AppendEnum appendedEnum, boolean isExecFromTest) {
		this.type = type;
		if(type.toString().equals(PathType.LOCAL.toString())){
			this.baseFilePath = localBasePath(isExecFromTest);
		} else {
			this.baseFilePath = wsFileBasePath();
		}
		this.appendedPath = appendedEnum.getProp();
	}
	
	//private methods
	private String wsFileBasePath(){
		StringBuilder birl = new StringBuilder();
		
		birl.append(JettyLauncher.protocol + "://");
		birl.append(JettyLauncher.host + ":");
		birl.append(JettyLauncher.port);
		
		birl.append(PropertySource.props.getProperty("resysclagem.file"));
		return birl.toString();
	}
	
	private String localBasePath(boolean isExecFromTest){
		StringBuilder birl = new StringBuilder();
		
		String absolutePath = new File(".").getAbsolutePath();
		absolutePath = absolutePath.substring(0, absolutePath.length()-2);//Remove the dot at the end of path
		
		birl.append(absolutePath); //pega a posição absoluta do projeto
		if(isExecFromTest)
			birl.append(File.separator + "src");
		birl.append(File.separator + PropertySource.props.getProperty("resysclagem.webapps"));		//no .properties deve ser setado sem a /
		birl.append(PropertySource.props.getProperty("resysclagem.file"));
		return birl.toString();
	}	

	//public methods
	public String getFullPath(){
		try {
			if(this.type.toString().equals(PathType.LOCAL.toString())){
				return Paths.get(this.baseFilePath + this.appendedPath).toString();
			} else {
				return this.baseFilePath + this.appendedPath;
			}
		} catch(InvalidPathException ipe){
			System.err.println("Invalid "+ this.type.toString() +" path: " + this.baseFilePath + this.appendedPath);
			throw ipe;
		}
	}
}


