package resysclagem.ws.classes;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Usuario {
	private int codU;
	private String nome;
	private String datanasc;
	private String email;
	private String senha;
	
	public Usuario(int codU, String nome, String datanasc, String email, String senha) {
		super();
		this.codU = codU;
		this.nome = nome;
		this.datanasc = datanasc;
		this.email = email;
		setSenha(senha);
	}

	public Usuario(String nome, String datanasc, String email, String senha) {
		super();
		this.nome = nome;
		this.datanasc = datanasc;
		this.email = email;
		setSenha(senha);
	}

	public Usuario(String email) {
		this.email = email;
	}

	public int getCodU() {
		return codU;
	}

	public void setCodU(int codU) {
		this.codU = codU;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDatanasc() {
		return datanasc;
	}
	
	public String getDatanascBD() {
		try {
			SimpleDateFormat brFormat = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat dbFormat = new SimpleDateFormat("yyyy-MM-dd");
			if(this.datanasc.contains("/")){
				return dbFormat.format(brFormat.parse(this.datanasc));
			} else {
				return this.getDatanasc();
			}
		} catch (ParseException e) {
		    e.printStackTrace();
		    return this.getDatanasc();
		}
	}

	public void setDatanasc(String datanasc) {
		this.datanasc = datanasc;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) { //throws Exception {
		//if(!isSenhaMd5(senha)) throw new Exception("senha insegura!");
		this.senha = senha;
	}
	
	private boolean isSenhaMd5(String senha)
	{
	    return senha.matches("[a-fA-F0-9]{32}");
	}
}
