package resysclagem.ws.utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import resysclagem.ws.classes.Dica;
import resysclagem.ws.classes.FilePath;
import resysclagem.ws.classes.FilePath.AppendEnum;
import resysclagem.ws.classes.FilePath.PathType;
import resysclagem.ws.classes.Residuo;

public class DicaJsonGetter {
	
	Gson gson;
	Type listStrType = new TypeToken<List<String>>(){}.getType();
	private Dica dicaAtualizada = null;
	private boolean success = false;
	private String message = "";
		
	public DicaJsonGetter(Dica dica) {
		gson = new Gson();
		FilePath fp = new FilePath(PathType.LOCAL, AppendEnum.DICA);
		String path = fp.getFullPath();
		String reutil = "-d";
		String completeFileName = "";
		try {
			if(dica.getIsReutilizavel())
				reutil = "-r";
			if(dica.getResiduo() == null)
				throw new Exception("JsonGetter: dica sem resíduo!");
			completeFileName = path + File.separator + dica.getResiduo().getCodR() + reutil;  //dica/codRediduo-r
		
			final File f = new File(completeFileName + ".json");
			String json = IOUtils.toString( new FileReader(f.getAbsolutePath()) );
			dica.setAnexos(
					gson.fromJson(json, listStrType)
					);
			this.success = true;
			this.dicaAtualizada = dica;
		} catch (FileNotFoundException fnfe){
			System.err.println("Atenção! arquivo JSON de dica não encontrado: " + completeFileName);
			this.message = fnfe.getMessage();
		} catch (IOException e) {
			e.printStackTrace();
			this.message = e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			this.message = e.getMessage();
		}
	}

	public Dica getDicaComAnexos() throws Exception {
		return dicaAtualizada;
	}	
	
	public boolean isSuccess(){
		return this.success;
	}
	
}
