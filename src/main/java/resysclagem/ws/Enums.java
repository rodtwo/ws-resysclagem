package resysclagem.ws;

public enum Enums {
	
	EXAMPLE("example"),
	HELLO("Heloo TCC!");
	public final String property;
	Enums(String prop){
		this.property = prop;
	}
	
	public String getProp(){
		return this.property;
	}
}
