package resysclagem.ws.services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

import resysclagem.ws.classes.Dica;
import resysclagem.ws.classes.FilePath;
import resysclagem.ws.classes.FilePath.AppendEnum;
import resysclagem.ws.classes.FilePath.PathType;
import resysclagem.ws.classes.MsgResponse;
import resysclagem.ws.classes.Request;
import resysclagem.ws.classes.Residuo;
import resysclagem.ws.dao.DicaDAO;
import resysclagem.ws.dao.ResiduoDAO;
import resysclagem.ws.launch.JettyLauncher;
import resysclagem.ws.utilities.DicaHtmlBuilder;

/**
 * REST endpoint para dicas.
 * 
 * @author TeamTCC
 *
 */
@Path("/dica")
public class DicaWS {

	Gson gson = new Gson();
	private DicaDAO dicaDAO; 

	//create
	/**
	 * Cadastro de nova dica. Só feita pelo Admin.
	 * 
	 * @param strDica
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response novaDica (String strRequest){
		Request request = gson.fromJson(strRequest, Request.class);
		// checar token
		if(!JettyLauncher.tokenClass.isAdminTokenValido(request)) {
			return Response.ok().entity(
					new MsgResponse("request", false, "Token inválido!")
					).build();
		}
		Dica dica = gson.fromJson(request.getObjeto(), Dica.class);
		dicaDAO = new DicaDAO();
		try {
			if(dica.getAnexos() == null || dica.getAnexos().isEmpty()){
				return Response.status(200)
						.entity(new MsgResponse("create", false, "Não existem anexos (imagens, textos) na dica!"))
						.build();
			}
			if(dica.getResiduo() != null){
				List<Dica> listDica = dicaDAO.buscarDicaDoResiduo(dica.getResiduo());
				if(!listDica.isEmpty()){
					for(Dica d : listDica){
						if( d.getIsReutilizavel() == dica.getIsReutilizavel() ){
							String tipo = (dica.getIsReutilizavel()) ? "reutilização" : "descarte";
							return Response.ok().entity(
									new MsgResponse("inserir", false, "Já existe uma dica de "
											+ tipo + " para o resíduo " + dica.getResiduo().getNome() + ", edite-a")
									).build();
						}
					}
				}
			}
			String isReutil = (dica.getIsReutilizavel()) ? "r" : "d";
			FilePath fp = new FilePath(PathType.SERVICE, AppendEnum.DICA);
			dica.setDescricaoPath(
					fp.getFullPath() + "/" + dica.getResiduo().getCodR() + "-" + isReutil + ".html"
					);
			Dica inserida = dicaDAO.inserirDica(dica);
			if (inserida != null){
				DicaHtmlBuilder htmlBuilder = new DicaHtmlBuilder(inserida);
				if(htmlBuilder.isSucesso()){
					return Response.status(201)
							.entity(new MsgResponse("create", true, 
													"Dica inserida com sucesso!", // htmlBuilder.getMsg()
													gson.toJson(inserida)
													)
							).build();
				} else {
					new Exception(gson.toJson(inserida)).printStackTrace();
					return Response.status(500)
							.entity(new MsgResponse("create", false, 
													"Dica não foi inserida!", htmlBuilder.getMsg())
							).build();
				}
			}
		} catch(SQLException sqle){
			sqle.printStackTrace();
			return Response.status(500)
					.entity(new MsgResponse("create", false, "Erro interno: " + sqle.getMessage()))
					.build();
		}catch (Exception e) {
			e.printStackTrace();
			return Response.status(500)
					.entity(new MsgResponse("create", false, "Erro interno: " + e.getMessage()))
					.build();
		}
		return null;
	}
	
	@GET
	@Path("/lista")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listaResiduo() throws SQLException, ClassNotFoundException {
    
		dicaDAO = new DicaDAO();
		
        try{
        	List<Dica> dicas = dicaDAO.listarDica();
	        if (!dicas.isEmpty()){
	        	return Response.ok().entity(
	        			gson.toJson(dicas)
	        			).build();			
	        } else {
	        	return Response.ok().entity(
	        			gson.toJson(new ArrayList<Dica>()) 	//vazio
	        			).build();
	        }
        } catch (SQLException e){
        	return Response.serverError().entity(
        			e.getMessage()
        			).build();
        } catch (Exception e){
        	e.printStackTrace();
        	return Response.serverError().build();
        }   
        
	}

	/**
	* WS para buscar dicas pelo resíduo. Pode ser acessado por admin e usuário.
	* 
	*/
	@POST
	@Path("/doresiduo")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscaDicaDeResiduo(String strRequest) 
    		  throws SQLException, ClassNotFoundException {
		Request request = gson.fromJson(strRequest, Request.class);
		// checar token
		if(!JettyLauncher.tokenClass.isTokenValido(request)) {
			return Response.ok().entity( 
					new MsgResponse("request", false, "Token inválido!")
					).build();
		}
		Residuo residuo = gson.fromJson(request.getObjeto(), Residuo.class);
		dicaDAO = new DicaDAO();
		try {
			List<Dica> listDica = dicaDAO.buscarDicaDoResiduo(residuo);
			if(listDica.isEmpty()){
	        	return Response.ok().entity(
	        			gson.toJson(new ArrayList<Dica>())
	        			).build();
			} else {
				return Response.ok().entity(
	        			gson.toJson(listDica)
	        			).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().entity(
	    			new MsgResponse("busca", false, "Erro do servidor: " + e.getMessage())
	    			).build();
		}
	}
	
	@GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscaDica(@QueryParam("nome") String nomeDica, 
    						  @QueryParam("cod") int codDica) throws SQLException, ClassNotFoundException {  
		
		dicaDAO = new DicaDAO();
		
        try{
        	Dica dica = dicaDAO.buscarDica(codDica, nomeDica);
	        if (dica != null){
	        	return Response.ok().entity(
	        			gson.toJson(dica)
	        			).build();			
	        } else {
	        	return Response.ok().entity(
	        			gson.toJson(new ArrayList<Dica>())
	        			).build();
	        }
        } catch (SQLException e){
        	e.printStackTrace();
        	return Response.serverError()
        			.entity(e.getMessage())
        			.build();
        } catch (Exception e){
        	e.printStackTrace();
        	return Response.serverError()
        			.entity(e.getMessage())
        			.build();

        }   
    }

	/**
	 * Para alterar, recebemos o cód. 
	 * 
	 * @param strRequest
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Response alterarDica(String strRequest) throws SQLException, ClassNotFoundException {  
		try {
			Request request = gson.fromJson(strRequest, Request.class);
			// checar token
			if(!JettyLauncher.tokenClass.isAdminTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			dicaDAO = new DicaDAO();
			Dica dica = gson.fromJson(request.getObjeto(), Dica.class);
		
			if(dica.getAnexos() == null || dica.getAnexos().isEmpty()){
				return Response.status(200)
						.entity(new MsgResponse("create", false, "Não existem anexos (imagens, textos) na dica!"))
						.build();
			}
			if(dica.getResiduo() != null){
				List<Dica> listDica = dicaDAO.buscarDicaDoResiduo(dica.getResiduo());
				if(!listDica.isEmpty()){
					for(Dica d : listDica){
						if( d.getIsReutilizavel() == dica.getIsReutilizavel() 
							&& dica.getCodD() != d.getCodD() // p/ sobreescrever se o cod for o msm 
							){
							//System.out.println(dica.getCodD() + " / " + d.getCodD());
							String tipo = (dica.getIsReutilizavel()) ? "reutilização" : "descarte";
							return Response.ok().entity(
									new MsgResponse("Alteração", false, "Já existe uma outra dica de "
											+ tipo + "!")
									).build();
						}
					}
				}
			}
			String isReutil = (dica.getIsReutilizavel()) ? "r" : "d";
			FilePath fp = new FilePath(PathType.SERVICE, AppendEnum.DICA);
			dica.setDescricaoPath(
					fp.getFullPath() + "/" + dica.getResiduo().getCodR() + "-" + isReutil + ".html"
					);
        	Dica alterada = dicaDAO.alterarDica(dica);
	        if (alterada != null){
	        	DicaHtmlBuilder htmlBuilder = new DicaHtmlBuilder(alterada);
				if(htmlBuilder.isSucesso()){
					return Response.status(201)
							.entity(new MsgResponse("Alteração", true, 
													"Dica alterada com sucesso!", //htmlBuilder.getMsg(),
													gson.toJson(alterada))
							).build();
				} else {
					new Exception(gson.toJson(alterada)).printStackTrace();
					return Response.status(500)
							.entity(new MsgResponse("Alteração", false, 
													"Dica não foi alterada: " + htmlBuilder.getMsg())
							).build();
				}
	        } else {
	        	throw new Exception("Não foi possível alterar!");
	        }
        } catch (SQLException e){
        	e.printStackTrace();
        	return Response.serverError()
        			.entity(e.getMessage())
        			.build();
        } catch (Exception e){
        	e.printStackTrace();
        	return Response.serverError()
        			.entity(e.getMessage())
        			.build();

        }   
    }
	
	
	@DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletaDica(@QueryParam("cod") Integer codDica,
    						   @QueryParam("id") String emailAdmin,
    						   @QueryParam("t") String token
    						  ) throws SQLException, ClassNotFoundException {  
		try {
			Request request = new Request(codDica.toString(), emailAdmin, token);
			// checar token
			if(!JettyLauncher.tokenClass.isAdminTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			dicaDAO = new DicaDAO();
			Dica dica = new Dica(codDica,"","",false);
			
        	boolean deletou = dicaDAO.removerDica(dica);
	        if (deletou){
	        	return Response.ok().entity(
	        			new MsgResponse("delete", true, "Dica removida!")
	        			).build();			
	        } else {
	        	return Response.ok().entity(
	        			new MsgResponse("delete", false, "Não pôde remover!")
	        			).build();
	        }
        } catch (SQLException e){
        	e.printStackTrace();
        	return Response.serverError()
        			.entity(e.getMessage())
        			.build();
        } catch (Exception e){
        	e.printStackTrace();
        	return Response.serverError()
        			.entity(e.getMessage())
        			.build();

        }   
    }
         
}
