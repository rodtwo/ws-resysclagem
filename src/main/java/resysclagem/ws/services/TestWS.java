package resysclagem.ws.services;


import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.jackson.JacksonFeature;

import com.google.gson.Gson;

import resysclagem.ws.PropertySource;
import resysclagem.ws.classes.Request;
import resysclagem.ws.classes.Usuario;
import resysclagem.ws.launch.JettyLauncher;

@Path("")
public class TestWS {
	
	Response response;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response ping(){
		String pjVersion = "pj.version: " + PropertySource.versionProps.getProperty("pj.version");
		String buildDate = "build.date: " + PropertySource.versionProps.getProperty("build.date");
		String[] info = new String[]{pjVersion, buildDate};
		return Response.status(200)
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
				.entity(info)
				.build();
	}
	
	//Teste de token - apagar
	@POST
	@Path("/getToken")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getToken(String strRequest) {
		Request request = new Gson().fromJson(strRequest, Request.class);
		return Response.status(201).entity(
					JettyLauncher.tokenClass.getUsuToken(request.getId())
					).build();
	}
	
	@POST
	@Path("/testToken")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response testToken(String strRequest) throws Exception {
		try {
			Request request = new Gson().fromJson(strRequest, Request.class);
			return Response.status(201).entity(
					JettyLauncher.tokenClass.isTokenValido(request)
					).build();
		} catch (Exception e) {
			System.err.println(strRequest);
			throw e;
		}
	}

}
