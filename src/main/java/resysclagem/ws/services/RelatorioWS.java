package resysclagem.ws.services;

import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import resysclagem.ws.classes.Administrador;
import resysclagem.ws.classes.KeyValue;
import resysclagem.ws.classes.MsgResponse;
import resysclagem.ws.classes.Relatorio;
import resysclagem.ws.classes.Request;
import resysclagem.ws.dao.AdministradorDAO;
import resysclagem.ws.dao.RelatorioDAO;
import resysclagem.ws.launch.JettyLauncher;
import resysclagem.ws.utilities.DicaJsonGetter;
import resysclagem.ws.utilities.Utility;

/**
 * Serviço de geração e retorno de relatórios para o módulo administrativo.
 * 
 * @author rodtw
 *
 */
@Path("/relatorio")
public class RelatorioWS {

    @Context
    private UriInfo context;

	Gson gson = new Gson();
	private RelatorioDAO relDAO;
	
	
	//create
	/**
	 * Recebe Request com objeto:String contendo JSON do relatório.
	 * Relatório só pode ser criado por outro admin.
	 * 
	 * Infos essenciais no JSON de relatório:
	 * - codRel: PK do relatório
	 * - dataE: dta de emissão
	 * - nome: nome do relatório
	 * Outros atributos podem variar de relatório para relatório.
	 * 
	 * @param strRequest
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response verTiposRelatorio() {		
		try {
			relDAO = new RelatorioDAO();
			List<KeyValue> tiposRelatorio = relDAO.listarTiposRelatorio(); 
			return Response.status(200)
						   .entity(tiposRelatorio)
						   .build();
			
		} catch (SQLException e) {
			e.printStackTrace();
			return Response
					.status(500)
					.entity(
							new MsgResponse("Tipos Rel", false, "Erro no sistema: " + e.getMessage())
					).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response
					.status(500)
					.entity(
							new MsgResponse("Tipos Rel", false, "Erro no sistema: " + e.getMessage())
					).build();
		}
		
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response gerarRelatorio(String strRequest) {		
		try {
			Gson gsonR = new GsonBuilder().disableHtmlEscaping().create(); 
			Request request = gsonR.fromJson(strRequest, Request.class);
			// checar token
			if(!JettyLauncher.tokenClass.isAdminTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			// gerar rel.
			Relatorio rel = gsonR.fromJson(request.getObjeto(), Relatorio.class);
			rel.fixQuery();
			relDAO = new RelatorioDAO();
			rel = relDAO.preGerarRelatorio(rel); 
			
			// System.out.println(gson.toJson(rel));
			
			return Response.status(200)
						   .entity(rel)
						   .build();
			
		} catch (SQLException e) {
			e.printStackTrace();
			return Response
					.status(500)
					.entity(
							new MsgResponse("cadastro", false, "Erro no sistema: " + e.getMessage())
					).build();
		} catch (Exception e) {
			System.out.println(strRequest);
			e.printStackTrace();
			return Response
					.status(500)
					.entity(
							new MsgResponse("cadastro", false, "Erro no sistema: " + e.getMessage())
					).build();
		}
		
	}
	
}
