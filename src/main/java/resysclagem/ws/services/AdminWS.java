package resysclagem.ws.services;

import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import com.google.gson.Gson;
import resysclagem.ws.classes.Administrador;
import resysclagem.ws.classes.MsgResponse;
import resysclagem.ws.classes.Request;
import resysclagem.ws.dao.AdministradorDAO;
import resysclagem.ws.launch.JettyLauncher;
import resysclagem.ws.utilities.Utility;


@Path("/admin")
public class AdminWS {

    @Context
    private UriInfo context;

	Gson gson = new Gson();
	private AdministradorDAO adminDAO;
	Response response;
	
	@GET
	@Path("/lista")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listaResiduos() throws SQLException, ClassNotFoundException {
        try{
        	adminDAO = new AdministradorDAO();
        	List<Administrador> admins = adminDAO.listarTodos();
	        if(!admins.isEmpty()){
	        	return Response.ok().entity(
	        			gson.toJson(admins)
	        			).build();			
	        } else {
	        	return Response.ok().entity(
	        			new MsgResponse("lista", false, "Não há nenhum administrador! Mas não seria esse um paradoxo?")
	        			).build();
	        }
        } catch (SQLException e){
        	return Response.serverError().entity(
        			e.getMessage()
        			).build();
        } catch (Exception e){
        	return Response.serverError().build();
        }   
        
	}
	
	/**
	 * Só pode ser criado por outro admin
	 * 
	 * @param strRequest
	 * @return
	 */
	@POST
	@Path("/cadastro")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response cadastro(String strRequest) {		
		try {
			// checar token
			Request request = gson.fromJson(strRequest, Request.class);
			if(!JettyLauncher.tokenClass.isAdminTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			Administrador admin = gson.fromJson(request.getObjeto(), Administrador.class);
			adminDAO = new AdministradorDAO();
			Administrador criado = adminDAO.inserirAdmin(admin);
			if(criado != null){
				//201 - created
				return Response.status(201)
						//.header("Access-Control-Allow-Origin", "*")
						.entity(new MsgResponse("Cadastro Admin", true, 
												admin.getNome() + " cadastrado!", 
												gson.toJson(criado) ))
						.build();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return Response
					.status(500)
					//.header("Access-Control-Allow-Origin", "*")
					.entity(
							gson.toJson(new MsgResponse("cadastro", false, e.getMessage())) 
					).build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response
				.status(500)
				//.header("Access-Control-Allow-Origin", "*")
				.entity(
						gson.toJson(new MsgResponse("cadastro", false, "Erro no sistema!"))//.constructJSON()
				).build();
	}

	/**
	 * Só pode ser alterado por admin Master OU pelo próprio admin
	 * 
	 * @param strAdmin
	 * @return
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response alterar (String strRequest){		
		// checar token
		Request request = gson.fromJson(strRequest, Request.class);
		if(!JettyLauncher.tokenClass.isAdminTokenValido(request)) {
			return Response.ok().entity( 
					new MsgResponse("request", false, "Token inválido!")
					).build();
		}
		
		Administrador admin = gson.fromJson(request.getObjeto(), Administrador.class);
		adminDAO = new AdministradorDAO();
		
		try {
			if ( ! request.getId().equals( adminDAO.consultarAdminMaster().getEmail() ) &&			// admin master 		
				 ! request.getId().equals(admin.getEmail())											// próprio admin 
			) {		
				System.out.println("request id: " + request.getId());
				System.out.println("admin alt: " + admin.getEmail());
				System.out.println("admin m: " + adminDAO.consultarAdminMaster().getEmail());
				return Response.ok().entity( 
						new MsgResponse("editar", false, "Você pode apenas editar o próprio usuário!")
						).build();
			}
			
			boolean created = adminDAO.alterarAdmin(admin);
			if(created){
				//201 - created
				return Response
						.ok()
						.entity(new MsgResponse("alterar", true, "Usuário alterado!"))
						.build();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return Response
					.status(200)
					.entity( new MsgResponse("alterar", false, "Erro: " + e.getMessage()) )
					.build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response
				.status(500)
				.entity(
						gson.toJson(new MsgResponse("alterar", false, "Erro no sistema!"))
				).build();
	}

	/**
	 * POST em /admin/session : login
	 * 
	 */
	@POST
    @Path("/session") ///{email}/{senha}
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login (String strAdmin) throws SQLException, ClassNotFoundException{
        Administrador adminReq = gson.fromJson(strAdmin, Administrador.class);
		boolean credential = false;
        try {
        	adminDAO = new AdministradorDAO();
            Administrador adminExiste = adminDAO.consultarAdmin(adminReq);
            Request request = new Request(gson.toJson(adminExiste), "", "");
        	ContaAdmin contaAdmin;
        	if(adminExiste != null){
        		credential = checkCredentials(adminReq, adminDAO);
        	} else {
        		//usuário não existe
        		contaAdmin = new ContaAdmin(false, false, request);
            	return Response.ok()
            			.entity(gson.toJson(contaAdmin))
            			.header("Access-Control-Allow-Origin", "*")
            			.build();
        	}
        	if (credential){
        		// dbUsuario existe, senha confere
        		request.setId(adminExiste.getEmail());
	            request.setToken( JettyLauncher.tokenClass.getAdminToken(adminExiste.getEmail()) );
        	}
        	return Response.ok()
            		.entity(gson.toJson(
            				new ContaAdmin(true, credential, request)))
            		.header("Access-Control-Allow-Origin", "*")
            		.build();

		} catch (Exception e) {
			e.printStackTrace();
	        return Response.status(500)
	        		.entity(e.getMessage())
	        		.header("Access-Control-Allow-Origin", "*")
	        		.build();
		}
        
    }
	
	private boolean checkCredentials(Administrador adm, AdministradorDAO usuDAO) 
			throws SQLException, ClassNotFoundException, Exception {
		boolean result = false;
		if(Utility.isNotNull(adm.getEmail()) && Utility.isNotNull(adm.getSenha())){
		  try {
			  adm = adminDAO.fazerLogin(adm);
			  if (adm != null) {
				  result = true;
			  }
		  } catch (SQLException e) {
			  System.out.println("Erro: " + e.getMessage());
		      result = false;
		  } catch (ClassNotFoundException e) {
		      System.out.println("Erro: " + e);
		      result = false;
		  } catch (Exception e) {
		      System.out.println("Erro: " + e);
		      result = false;
		  }
		}else{
		    result = false;
		}
		return result;
	}
	
	/*
	 * DELETE into /session : logout
	 */
	@DELETE
    @Path("/session") ///{email}/{senha}
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response logout(Administrador admin) throws SQLException, ClassNotFoundException{
		// TO DO
		return Response.status(418).build();
	}

	/**
	 * Só pode ser feito por admin.
	 * DELETE /admin?cod=0&id=y&t=z: remover admin
	 * 
	 */
	@DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response remover (@QueryParam("cod") Integer cod, 	// cod do admin deletado
    						 @QueryParam("id") String id,		// email do admin logad
    						 @QueryParam("t") String token) throws SQLException, ClassNotFoundException {
		try {
			// checar token
			Request request = new Request(cod.toString(), id, token);
			if(!JettyLauncher.tokenClass.isAdminTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			Administrador codAdmin = new Administrador(cod, "", "");
			adminDAO = new AdministradorDAO();
			Administrador admin = adminDAO.consultarAdmin(codAdmin);

			// barrar delete caso tente deletar admin mater
			if ( admin.getEmail().equals(adminDAO.consultarAdminMaster().getEmail())	) {
				
				return Response.ok().entity( 
						new MsgResponse("delete", false, "Administrador mestre não pode ser deletado!")
						).build();
			}
			// barrar delete caso não seja o próprio usário (mas Adm master ainda pode)
			if ( ! request.getId().equals(admin.getEmail())	&& 
				 ! request.getId().equals(adminDAO.consultarAdminMaster().getEmail()) ) {				
				return Response.ok().entity( 
						new MsgResponse("delete", false, "Você pode apenas deletar o próprio usuário!")
						).build();
			}
			
			if(admin != null) {
				if(adminDAO.removerAdmin(admin)){
					return Response.ok(
							new MsgResponse("delete", true, "Usuário "+ admin.getNome() +" deletado!")
							).build();
				}
			}
			return Response.ok(
					new MsgResponse("delete", false, "Usuário não foi deletado!")
					).build();
		} catch (SQLException sqle){
			sqle.printStackTrace();
			return Response.status(500)
						   .entity(sqle.getMessage())
						   .build();
		} catch (Exception e){
			e.printStackTrace();
			return Response.status(500)
					   	   .entity(e.getMessage())
					   	   .build();
		}
	}
	
	public class ContaAdmin {
		private boolean loginExists;
		private boolean passwordConfirmed;
		private Request request;
		
		public ContaAdmin(boolean loginExists, boolean passwordConfirmed, Request request){
			this.loginExists = loginExists;
			this.passwordConfirmed = passwordConfirmed;
			this.request = request;
		}
		
		public boolean getLoginExists() {
			return loginExists;
		}

		public void setLoginExists(boolean loginExists) {
			this.loginExists = loginExists;
		}

		public boolean getPasswordConfirmed() {
			return passwordConfirmed;
		}

		public void setPasswordConfirmed(boolean passwordConfirmed) {
			this.passwordConfirmed = passwordConfirmed;
		}
		
		public Request getRequest() {
			return request;
		}

		public void setRequest(Request request) {
			this.request = request;
		}
	}

}
