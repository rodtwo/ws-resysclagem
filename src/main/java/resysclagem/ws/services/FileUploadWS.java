package resysclagem.ws.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.EnumSet;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FilenameUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

import com.fasterxml.jackson.core.filter.TokenFilter;
import com.google.gson.Gson;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;
import resysclagem.ws.PropertySource;
import resysclagem.ws.classes.Dica;
import resysclagem.ws.classes.FilePath;
import resysclagem.ws.classes.Residuo;
import resysclagem.ws.classes.FilePath.AppendEnum;
import resysclagem.ws.classes.FilePath.PathType;
import resysclagem.ws.classes.MsgResponse;
import resysclagem.ws.classes.Request;
import resysclagem.ws.dao.DicaDAO;
import resysclagem.ws.dao.ResiduoDAO;
import resysclagem.ws.launch.JettyLauncher;

/**
 * REST endpoint para upload de imagens.
 * 
 * @author TeamTCC
 *
 */
@Path("/file")
public class FileUploadWS {

	Gson gson = new Gson();

	private enum TypeUpload {	
		DICA("DIC"),
		PONTOCOLETA("PCO"),
		RESIDUO("RES");		//RESIDUO_SUB("RES_SUB"); non ecziste mais
		public final String property;
		TypeUpload(String prop){
			this.property = prop;
		}
		public String getProp(){
			return this.property;
		}
	}
	
	/**
	 * Esse método de upload de imagens deve ser chamado logo após a inserção/upload.
	 * 
	 * @param stream
	 * @param fileDetail
	 * @param tipo
	 * @param subpasta
	 * @return
	 * @throws Exception
	 */
	@Path("/fileUpload")
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response postMsg (
						@FormDataParam("id") String admEmail,
						@FormDataParam("t") String token,
	    				@FormDataParam("file") InputStream stream,
	    				@FormDataParam("file") FormDataContentDisposition fileDetail,
						@FormDataParam("tipo") String tipo,
						@FormDataParam("cod") String cod
						) throws Exception 
	{
		try {
			
			Request request = new Request("", admEmail, token);
			
			//BUG usando JPG e JPEG
			String extension = FilenameUtils.getExtension(fileDetail.getFileName()).toLowerCase();
			if(extension.contains("jpeg")){ 
				extension = extension.replace("jpeg", "jpg");
			}
			
			// checar token
			if(!JettyLauncher.tokenClass.isAdminTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			
			String uploadedFileLocation = null;
			String uploadedMiniFileLocation = null;
			FilePath fp = null;
			
			if(stream == null || fileDetail == null){
				return Response.status(200)
						.entity(new MsgResponse("fileUpload", false, "Nenhum arquivo foi selecionado para upload!"))
						.build();
			}
			
			// DICA
			if(tipo.contains(TypeUpload.DICA.getProp())){
				String pkDica = cod;
				fp = new FilePath(PathType.LOCAL, AppendEnum.DICA);
				uploadedFileLocation = fp.getFullPath()
						+ File.separator + pkDica			// PK da dica
						+ File.separator + fileDetail.getFileName();
			// PONTO
			} else if (tipo.contains(TypeUpload.PONTOCOLETA.getProp())) {
				String pkPCO = cod;
				fp = new FilePath(PathType.LOCAL, AppendEnum.PONTOCOLETA);
				uploadedFileLocation = fp.getFullPath()
						+ File.separator + pkPCO
						+ File.separator + fileDetail.getFileName();
			// RESIDUO
			} else if (tipo.contains(TypeUpload.RESIDUO.getProp())) {
				String pkRES = cod;
				fp = new FilePath(PathType.LOCAL, AppendEnum.RESIDUO);
				uploadedFileLocation = fp.getFullPath() 
						+ File.separator + pkRES
						+ "-big." + extension;
				uploadedMiniFileLocation = fp.getFullPath() 
						+ File.separator + pkRES 
						+ "-mini." + extension;
			} else {
				System.err.println("Tipo inválido!");
				return Response.status(200)
						.entity(new MsgResponse("fileUpload", false, "Tipo de upload não definido! É uma resíduo (RES), ponto de coleta (PCO) etc?"))
						.build();
			}
			
	        // save it
			boolean success = false;
			String output = "";
			String extra = "";
					
	        try{
	        	writeToFile(stream, uploadedFileLocation);
	        	output = "Upload feito com sucesso!";
	        	extra = "File uploaded to : " + uploadedFileLocation;
	        	success = true;
	        } catch(Exception e){
	        	e.printStackTrace();
	        	output = "Error uploading: " + e.getMessage(); 
	        }
	        try {
	        	if(uploadedMiniFileLocation != null){
			        Thumbnails.of(uploadedFileLocation)
				        .size(80, 80)
				        .toFile(uploadedMiniFileLocation);
	        	}
	        } catch (Exception e){
	        	System.err.println("Erro ao redimensionar imagem!");
	        	e.printStackTrace();
	        }
	        
	        /*
	        final Application application = new ResourceConfig()
	                .packages("org.glassfish.jersey.examples.multipart")
	                .register(MultiPartFeature.class);
			*/
	        return Response.status(200).
	        		entity(new MsgResponse("fileUpload", success, output, extra))
	        		.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
	        		.build();
		} catch(Exception e){
			e.printStackTrace();
			return Response.status(500).
	        		entity(new MsgResponse("fileUpload", false, e.getMessage(), ""))
	        		.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
	        		.build();
		}

	}

	private void writeToFile(InputStream uploadedInputStream,
	            			 String uploadedFileLocation) throws Exception {

	            try {
	            	File file = new File(uploadedFileLocation);
	            	Files.createDirectories(Paths.get(file.getParentFile().getAbsolutePath()));	            	
	                OutputStream out;
	                int read = 0;
	                byte[] bytes = new byte[64000];

	                out = new FileOutputStream(new File(uploadedFileLocation), false);
	                while ((read = uploadedInputStream.read(bytes)) != -1) {
	                    out.write(bytes, 0, read);
	                }
	                out.flush();
	                out.close();
	        
	            } catch (Exception e) {
	            	System.out.println(uploadedFileLocation);
	            	e.printStackTrace();
	            	throw new Exception(e.getMessage());
	            }

	}

	/*
    public static void copyFile(File source, File destination) throws IOException {
        if (destination.exists())
            destination.delete();
        FileChannel sourceChannel = null;
        FileChannel destinationChannel = null;
        try {
            sourceChannel = new FileInputStream(source).getChannel();
            destinationChannel = new FileOutputStream(destination).getChannel();
            sourceChannel.transferTo(0, sourceChannel.size(),
                    destinationChannel);
        } finally {
            if (sourceChannel != null && sourceChannel.isOpen())
                sourceChannel.close();
            if (destinationChannel != null && destinationChannel.isOpen())
                destinationChannel.close();
       }
       */
}

