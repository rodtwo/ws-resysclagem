package resysclagem.ws.services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import resysclagem.ws.classes.Administrador;
import resysclagem.ws.classes.Categoria;
import resysclagem.ws.classes.MsgResponse;
import resysclagem.ws.classes.Request;
import resysclagem.ws.classes.Residuo;
import resysclagem.ws.classes.Usuario;
import resysclagem.ws.dao.CategoriaDAO;
import resysclagem.ws.launch.JettyLauncher;
import resysclagem.ws.services.UsuarioWS.ContaUsuario;

/**
 * REST endpoint para resíduos. O ideal é que só seja chamado 
 * quando há mudanças no banco, e que os resíduos sejam guardados 
 * localmente no Android em cache.
 * 
 * @author TeamTCC
 *
 */
@Path("/categoria")
public class CategoriaWS {

	Gson gson = new Gson();
	private CategoriaDAO categoriaDAO;
	
	//create
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response inserirResiduo(String strRequest){
		try {
			// checar token
			Request request = gson.fromJson(strRequest, Request.class);
			if(!JettyLauncher.tokenClass.isAdminTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			Categoria categoria = gson.fromJson(request.getObjeto(), Categoria.class);
			categoriaDAO = new CategoriaDAO();
			List<Categoria> catExiste = categoriaDAO.buscaExataCategoria(categoria);
			
			//se resíduo com mesmo nome já existir
			if(!catExiste.isEmpty()){
				return Response.status(201)
						.entity(new MsgResponse("cadastro", false, "Existe uma categoria de mesmo nome, tente um nome diferente!"))
						.build();
			}		
			Categoria created = categoriaDAO.inserirCat(categoria);
			if(created != null){
				return Response.ok( new MsgResponse("cadastro", true, "Categoria inserida!", gson.toJson(created)) )
							   .build();
			}
		} catch (SQLException e) {
			return Response.status(500).entity(new MsgResponse("cadastro", false, "500: Erro no sistema! \n" + e.getMessage())).build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.status(500).entity(new MsgResponse("cadastro", false, "500: Erro no sistema!")).build();
	}
	
	//read
	/*
	@GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscaResiduo(@QueryParam("busca") String busca, @QueryParam("cod") String cod) {  
		residuoDAO = new ResiduoDAO();
		List<Residuo> residuos = new ArrayList<Residuo>();
        try{
        	if(cod != null){
        		Residuo residuo = residuoDAO.pesquisarResiduo(Integer.valueOf(cod));
        		return Response.ok().entity(
	        			gson.toJson(residuos.add(residuo))
	        			).build();
        	} else if (busca != null) {
        		residuos = residuoDAO.buscarResiduo(new Residuo(busca, "", "", ""));
    	        if(!residuos.isEmpty()){
    	        	return Response.ok().entity(
    	        			gson.toJson(residuos)
    	        			).build();			
    	        } else {
    	        	return Response.ok().entity(
    	        			new MsgResponse("busca", false, "Não há nenhum resíduo!")
    	        			).build();
    	        }
        	} else {
        		return Response.status(412).build();
        	}
        } catch (SQLException e){
        	return Response.serverError().entity(
        			e.getMessage()
        			).build();
        } catch (Exception e){
        	return Response.serverError().build();
        }   
    }
	*/
	
	//read
	@GET
	@Path("/lista")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listaCategorias() throws SQLException, ClassNotFoundException {
        
        try {
        	
        	categoriaDAO = new CategoriaDAO();
		
        	List<Categoria> categorias = categoriaDAO.listarCategorias();
	        if(!categorias.isEmpty()){
	        	return Response.ok().entity(
	        			gson.toJson(categorias)
	        			).build();			
	        } else {
	        	return Response.ok().entity(
	        			gson.toJson(new ArrayList<Categoria>())		//json array vazio... talvez uma msg fosse melhor?
	        			).build();
	        }
        } catch (SQLException e){
        	e.printStackTrace();
        	return Response.serverError().entity(
        			e.getMessage()
        			).build();
        } catch (Exception e){
        	e.printStackTrace();
        	return Response.serverError().entity(
        			e.getMessage()
        			).build();
        }   
        
	}
	
	//update
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response alterar (String strRequest){				
		try {
			// checar token
			Request request = gson.fromJson(strRequest, Request.class);
			if(!JettyLauncher.tokenClass.isAdminTokenValido(request)) {
				return Response.ok().entity( 
						new MsgResponse("request", false, "Token inválido!")
						).build();
			}
			Categoria cat = gson.fromJson(request.getObjeto(), Categoria.class);
			categoriaDAO = new CategoriaDAO();
			
			boolean changed = categoriaDAO.alterarCategoria(cat);
			if(changed){
				return Response
						.ok()
						.entity(new MsgResponse("alterar", true, "Categoria alterada!"))
						.build();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return Response
					.status(200)
					.entity( new MsgResponse("alterar", false, "Erro: " + e.getMessage()) )
					.build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response
				.status(500)
				.entity(
						gson.toJson(new MsgResponse("alterar", false, "Erro no sistema!"))
				).build();
	}
	
	//delete
	/**
	 * Delete de categoria só pode ser feito por admin.
	 * 
	 * @param strCategoria
	 * @return
	 */
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response remover (@QueryParam("cod") Integer cod, 	// cod da categoria deletada
							 @QueryParam("id") String id,		// email do admin logado
							 @QueryParam("t") String token) {
		// checar token
		Request request = new Request(cod.toString(), id, token);
		if(!JettyLauncher.tokenClass.isAdminTokenValido(request)) {
			return Response.ok().entity( 
					new MsgResponse("request", false, "Token inválido!")
					).build();
		}
		categoriaDAO = new CategoriaDAO();
		try {
			Categoria cat = categoriaDAO.pesquisarCategoria(Integer.valueOf(cod));
			if(cat != null) {
				boolean deleted = categoriaDAO.removerCategoria(cat);
				if(deleted){
					return Response
							.ok()
							.entity(new MsgResponse("remover", true, "Categoria removida!"))
							.build();
				}
			} else {
				return Response
						.ok()
						.entity(new MsgResponse("remover", false, "Categoria não existe!"))
						.build();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return Response
					.status(200)
					.entity( new MsgResponse("remover", false, "Erro na base: " + e.getMessage()) )
					.build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response
				.status(500)
				.entity(
						gson.toJson(new MsgResponse("remover", false, "Erro interno do servidor!"))
				).build();
	}
	
        
}
