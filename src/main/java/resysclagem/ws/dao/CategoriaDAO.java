package resysclagem.ws.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.text.AttributeSet.CharacterAttribute;

import resysclagem.ws.classes.Categoria;

public class CategoriaDAO {
	private final String stmtInserirC = "INSERT INTO categoria (nome, cor) VALUES (?,?)";
    private final String stmtAlterarC = "UPDATE categoria SET nome=?, cor=? WHERE codc=?";
    private final String stmtRemoverC = "UPDATE categoria SET deletado=true WHERE codc=?";
    private final String stmtRemoverDefC = "DELETE FROM categoria WHERE codc=?";
    private final String stmtListarC = "SELECT * FROM categoria WHERE deletado=false";
    private final String stmtPesquisaAltC = "SELECT * FROM categoria WHERE codc=? AND deletado=false";
    private final String stmtBuscarC = "SELECT * FROM categoria WHERE nome LIKE ? AND deletado=false ORDER BY nome";
    private final String stmtBuscaExataC = "SELECT * FROM categoria WHERE nome=? AND deletado=false";
    
    public Categoria inserirCat(Categoria categoria) throws SQLException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        Categoria inserida = null;
        try{
            con = ConnectionFactory.getConnection();
            con.setAutoCommit(false);
            stmt = con.prepareStatement(stmtInserirC,PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, categoria.getNome());
            stmt.setString(2, categoria.getCor());
            stmt.executeUpdate();
            con.commit();
            
            ResultSet rs = stmt.getGeneratedKeys();  
            if(rs.next())
            	categoria.setCodC(rs.getInt(1));
            	inserida = categoria;
            rs.close();
            
            return inserida;
        } catch (SQLException ex) {
            try{con.rollback();}catch(SQLException ex1){System.out.println("Erro ao tentar rollback. Ex="+ex1.getMessage());};
            throw new RuntimeException("Erro ao inserir um residuo no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{stmt.close();}catch(Exception ex){System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());};
            try{con.close();;}catch(Exception ex){System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());};
        }
    }
    
    public boolean alterarCategoria(Categoria categoria) throws SQLException, ClassNotFoundException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtAlterarC);
            stmt.setString(1, categoria.getNome());
            stmt.setString(2, categoria.getCor());
            stmt.setInt(3, categoria.getCodC());
            stmt.executeUpdate();
            
            return true;
        }catch(SQLException ex){
            throw new SQLException("Erro ao alterar o residuo. Origem: " + ex.getMessage());
        }finally{
            try{stmt.close();}catch(Exception ex){
            	System.out.println("Erro ao fechar stmt. Ex = " +ex.getMessage()); throw ex;};
            try{con.close();;}catch(Exception ex){
            	System.out.println("Erro ao fechar conexão. Ex= "+ex.getMessage()); throw ex;};
        }
    }
    
    public boolean removerCategoria(Categoria categoria) throws SQLException, ClassNotFoundException, Exception{
        Connection con = null;
        PreparedStatement stmt = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtRemoverC);
            stmt.setInt(1, categoria.getCodC());
            stmt.executeUpdate();
            return true;
        }catch(SQLException ex){
            throw new SQLException("Erro ao deletar o residuo. Origem: " +ex.getMessage());
        }finally{
            try{stmt.close();}catch(Exception ex){
            	System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage()); throw ex;};
            try{con.close();;}catch(Exception ex){
            	System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage()); throw ex;};                
        }
    }
    
    public List<Categoria> listarCategorias() throws SQLException, ClassNotFoundException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Categoria> lista = new ArrayList<Categoria>();
        try {
            con = ConnectionFactory.getConnection();
	        stmt = con.prepareStatement(stmtListarC);
	        rs = stmt.executeQuery();
	        while(rs.next()){
	    		Categoria categoria = new Categoria(rs.getInt("codc"), rs.getString("nome"), rs.getString("cor"));
	            lista.add(categoria);
	        }
	        return lista;
        } catch (SQLException e){
        	throw new SQLException(e.getMessage());
        } catch (ClassNotFoundException ex) {
            throw new Exception("Erro ao listar os residuos: " + ex.getMessage());
	    } catch (Exception e) {
	    	e.printStackTrace();
	        throw new Exception(e.getMessage());
	    }
    }
    
    public Categoria pesquisarCategoria(int codC) throws SQLException, Exception { // utilizada na alteração(?)
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Categoria categoria = null;
        try{
        	con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtPesquisaAltC);
            stmt.setInt(1, codC);
            rs = stmt.executeQuery();
            if(rs.next()){
            	categoria = new Categoria(rs.getInt("codc"), rs.getString("nome"), rs.getString("cor"));
            }
            return categoria;
        }catch(SQLException ex){
            throw new SQLException("Erro ao pesquisar o cliente. Origem=" +ex.getMessage());
        } catch (Exception e){
        	throw new Exception(e.getMessage());
        }finally{
            try{rs.close();}catch(Exception ex){System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());};
            try{stmt.close();}catch(Exception ex){System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());};
            try{con.close();;}catch(Exception ex){System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());};                
        }
    }
     
    public List<Categoria> buscarCategoria(Categoria categoria) throws SQLException, ClassNotFoundException{ // --> campo de busca nome
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Categoria> lista = new ArrayList<Categoria>();
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtBuscarC);
            stmt.setString(1, "%"+ categoria.getNome() +"%");
            rs = stmt.executeQuery();
            while(rs.next()){
            	categoria = new Categoria(rs.getInt("codc"), rs.getString("nome"), rs.getString("cor"));
            	lista.add(categoria);
            }
            return lista;
        }catch(SQLException ex){
            throw new SQLException("Erro ao buscar cliente. Origem: " + ex.getMessage());
        }finally{
            try{rs.close();}catch(Exception ex){System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());};
            try{stmt.close();}catch(Exception ex){System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());};
            try{con.close();;}catch(Exception ex){System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());};
        }
    }
    
    public List<Categoria> buscaExataCategoria(Categoria categoria) throws ClassNotFoundException { 
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Categoria> lista = new ArrayList<Categoria>();
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtBuscaExataC);
            stmt.setString(1, categoria.getNome());
            rs = stmt.executeQuery();
            while(rs.next()){
            	categoria = new Categoria(rs.getInt("codc"), rs.getString("nome"), rs.getString("cor"));
            	lista.add(categoria);
            }
            return lista;
        }catch(SQLException ex){
            throw new RuntimeException("Erro ao buscar resíduo. Origem: "+ex.getMessage());
        }finally{
            try{rs.close();}catch(Exception ex){System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());};
            try{stmt.close();}catch(Exception ex){System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());};
            try{con.close();;}catch(Exception ex){System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());};
        }
    }
}
