package resysclagem.ws.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import resysclagem.ws.classes.Dica;
import resysclagem.ws.classes.Residuo;
import resysclagem.ws.classes.Usuario;
import resysclagem.ws.utilities.DicaJsonGetter;

public class DicaDAO {
	private final String stmtInserirD = "INSERT INTO tutorial (nome, imagem_descricao, isreutilizavel, residuo_codr) VALUES (?,?,?,?)";
    private final String stmtAlterarD = "UPDATE tutorial SET nome=?, imagem_descricao=?, isreutilizavel=?, residuo_codr=? WHERE codt=?";
    private final String stmtRemoverD = "DELETE FROM tutorial WHERE codt=?";
    private final String stmtBuscarD = "SELECT * FROM tutorial WHERE codt=? OR nome=?";
    private final String stmtBuscarDoResiduo = "SELECT * FROM tutorial WHERE residuo_codr=?";
    private final String stmtListarD = "SELECT * FROM tutorial";
    
    public Dica inserirDica(Dica dica) throws SQLException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        Dica inserida = null;
        try{
            con = ConnectionFactory.getConnection();
            con.setAutoCommit(false);
            stmt = con.prepareStatement(stmtInserirD,PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, dica.getNome());
            stmt.setString(2, dica.getDescricaoPath());
            stmt.setBoolean(3, dica.getIsReutilizavel());
            stmt.setInt(4, dica.getResiduo().getCodR());
            stmt.executeUpdate();
            con.commit();
            
            ResultSet rs = stmt.getGeneratedKeys();  
            if(rs.next())
            	dica.setCodD(rs.getInt(1));
            rs.close();
            
            inserida = dica;
            return inserida;
        } catch (SQLException ex) {
            try{con.rollback();}catch(SQLException ex1){
            			System.out.println("Erro ao tentar rollback. Ex="+ex1.getMessage());
            			ex1.printStackTrace(); };
            throw new SQLException("Erro ao inserir uma dica no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{stmt.close();}catch(Exception ex){
            	System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            	throw ex; };
            try{con.close();;}catch(Exception ex){
            	System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            	throw ex; };
        }
    }
    
    public Dica alterarDica(Dica dica) throws ClassNotFoundException{
        Connection con = null;
        PreparedStatement stmt = null;
        Dica alterada = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtAlterarD);
            stmt.setString(1, dica.getNome());
            stmt.setString(2, dica.getDescricaoPath());
            stmt.setBoolean(3, dica.getIsReutilizavel());
            stmt.setInt(4, dica.getResiduo().getCodR());
            stmt.setInt(5, dica.getCodD());
            stmt.executeUpdate();
            
            alterada = dica;
            return alterada;
        }catch(SQLException ex){
            throw new RuntimeException("Erro ao alterar a dica. Origem = " +ex.getMessage());
        }finally{
            try{stmt.close();}catch(Exception ex){System.out.println("Erro ao fechar stmt. Ex = " +ex.getMessage());};
            try{con.close();;}catch(Exception ex){System.out.println("Erro ao fechar conexão. Ex= "+ex.getMessage());};
        }
    }
    
    public boolean removerDica(Dica dica) throws Exception{
        Connection con = null;
        PreparedStatement stmt = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtRemoverD);
            stmt.setInt(1, dica.getCodD());
            stmt.executeUpdate();
            
            return true;
        }catch(SQLException ex){
        	System.err.println("Erro ao deletar a dica. Origem: " +ex.getMessage());
        	throw ex;
        }finally{
            try{stmt.close();}catch(Exception ex){
            	System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            	throw ex;
            	};
            	
            try{con.close();;}catch(Exception ex){
            	System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            	throw ex;
            	};                
        }
    }

    public List<Dica> buscarDicaDoResiduo(Residuo residuo) throws Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Dica> listDica = new ArrayList<Dica>();
        try {
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtBuscarDoResiduo);
            stmt.setInt(1, residuo.getCodR());
            rs = stmt.executeQuery();
        } catch (ClassNotFoundException ex) {
            throw ex;
        }
        while(rs.next()) {
        	Dica dica = new Dica(rs.getInt("codt"), rs.getString("nome"), rs.getString("imagem_descricao"), rs.getBoolean("isreutilizavel"));
        	dica.setResiduo(new ResiduoDAO().pesquisarResiduo(rs.getInt("residuo_codr")));
        	listDica.add(dica);
        }
        return listDica;
    }

    
    public Dica buscarDica(int codDica, String nomeDica) throws Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Dica dica = null;
        try {
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtBuscarD);
            stmt.setInt(1, codDica);
            stmt.setString(2, nomeDica);
            rs = stmt.executeQuery();
        } catch (ClassNotFoundException ex) {
            throw ex;
        }
        while(rs.next()) {
        	dica = new Dica(rs.getInt("codt"), rs.getString("nome"), rs.getString("imagem_descricao"), rs.getBoolean("isreutilizavel"));
        	dica.setResiduo(new ResiduoDAO().pesquisarResiduo(rs.getInt("residuo_codr")));
        	break;
        }
        return dica;
    }
    
    public List<Dica> listarDica() throws SQLException, ClassNotFoundException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Dica> lista = new ArrayList<Dica>();
        try {
            con = ConnectionFactory.getConnection();
	        stmt = con.prepareStatement(stmtListarD);
	        rs = stmt.executeQuery();
	        while(rs.next()){ //construtor já tá arrumado beca haha
	        	Dica dica = new Dica(rs.getInt("codt"), rs.getString("nome"), rs.getString("imagem_descricao"), rs.getBoolean("isreutilizavel"));
	        	// setar residuo da dica
	        	dica.setResiduo( new ResiduoDAO().pesquisarResiduo(rs.getInt("residuo_codr")) );
	        	// setar anexos da dica
	        	DicaJsonGetter jsonGetter = new DicaJsonGetter(dica);
	        	if(jsonGetter.isSuccess())
	        		dica = jsonGetter.getDicaComAnexos();
	        	lista.add(dica);
	        }
	        return lista;
        } catch (SQLException sqle){
        	throw sqle;
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            throw ex;
	    } catch (Exception e){
	    	throw e;
	    }
    }
    
}