package resysclagem.ws.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import resysclagem.ws.classes.Categoria;
import resysclagem.ws.classes.FilePath;
import resysclagem.ws.classes.Residuo;
import resysclagem.ws.classes.Usuario;
import resysclagem.ws.classes.FilePath.AppendEnum;
import resysclagem.ws.classes.FilePath.PathType;

public class ResiduoDAO {
	
	private final String stmtInserirR = "INSERT INTO residuo (nome, descricao, imagem_residuo, categoria_codc) VALUES (?,?,?,?)";
    private final String stmtAlterarR = "UPDATE residuo SET nome=?, descricao=?, imagem_residuo=?, categoria_codc=? WHERE codr=?";
    public static final String stmtAlterarImagem = "UPDATE residuo SET imagem_residuo=? WHERE codr=?";
    private final String stmtRemoverR = "DELETE FROM residuo WHERE codr=?";
    private final String stmtSetRemovido = "UPDATE residuo SET deletado=true WHERE codr=?";
    private final String stmtListarR = "SELECT * FROM residuo WHERE deletado=false";
    private final String stmtBuscaPelaCat = "SELECT * FROM residuo WHERE categoria_codc=? AND deletado=false";
    private final String stmtPesquisaAltR = "SELECT * FROM residuo WHERE codr=? AND deletado=false";
    private final String stmtBuscarR = "SELECT * FROM residuo WHERE nome LIKE ? ORDER BY nome";
    private final String stmtBuscaExata = "SELECT * FROM residuo WHERE nome=? AND deletado=false";
    private final String pkConsulta = "SELECT `AUTO_INCREMENT` FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'residuo' ";
    private FilePath fp = new FilePath(PathType.SERVICE, AppendEnum.RESIDUO);
    
    public Residuo inserirResiduo(Residuo residuo) throws SQLException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        PreparedStatement stmt2 = null;
        Residuo inserido = null;
        try{
            con = ConnectionFactory.getConnection();
            con.setAutoCommit(false);
            stmt = con.prepareStatement(stmtInserirR, PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, residuo.getNome());
            stmt.setString(2, residuo.getDescricao());
            stmt.setString(3, residuo.getImagemResiduo());
            stmt.setInt(4, residuo.getCategoria().getCodC());
            stmt.executeUpdate();
            
            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()){
            	residuo.setCodR(rs.getInt(1));
				residuo.imageNameStandard(fp);		// deixa 
                stmt2 = con.prepareStatement(stmtAlterarImagem, PreparedStatement.RETURN_GENERATED_KEYS);
                stmt2.setString(1, residuo.getImagemResiduo());
                stmt2.setInt(2, rs.getInt(1));
                stmt2.executeUpdate();
                inserido = residuo;
            }
            stmt.close();
            stmt2.close();
            con.commit();
            return inserido;
        } catch (SQLException ex) {
            try{
            	con.rollback();
            }catch(SQLException ex1){System.out.println("Erro ao tentar rollback. Ex="+ex1.getMessage());};
            throw new RuntimeException("Erro ao inserir um residuo no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{stmt.close();}catch(Exception ex){System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());};
            try{con.close();;}catch(Exception ex){System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());};
        }
    }
    
    public int pegarProximaPrimaryKey() throws Exception{
        Connection con = null;
        PreparedStatement stmt = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(pkConsulta);
            stmt.executeQuery();
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
            	return rs.getInt(1);
            }
        }catch (SQLException sqle) {
        	sqle.printStackTrace();
			throw new Exception(sqle.getMessage());
        }catch (Exception e) {
        	e.printStackTrace();
			throw new Exception(e.getMessage());
		}
        return 0;
    }
    
    public Residuo alterarResiduo(Residuo residuo) throws SQLException, ClassNotFoundException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtAlterarR);
            stmt.setString(1, residuo.getNome());
            stmt.setString(2, residuo.getDescricao());
            residuo.imageNameStandard(fp);
            stmt.setString(3, residuo.getImagemResiduo());
            stmt.setInt(4, residuo.getCategoria().getCodC());
            stmt.setInt(5, residuo.getCodR());
            stmt.executeUpdate();
            
            return residuo;
        }catch(SQLException ex){
            throw new SQLException("Erro ao alterar o residuo. Origem: " + ex.getMessage());
        }finally{
            try{stmt.close();}catch(Exception ex){
            	System.out.println("Erro ao fechar stmt. Ex = " +ex.getMessage()); throw ex;};
            try{con.close();;}catch(Exception ex){
            	System.out.println("Erro ao fechar conexão. Ex= "+ex.getMessage()); throw ex;};
        }
    }

    //"desativar" o resíduo, ao invés de deletar da base
    public boolean setRemovidoResiduo(Residuo residuo) throws SQLException, ClassNotFoundException, Exception{
        Connection con = null;
        PreparedStatement stmt = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtSetRemovido);
            stmt.setInt(1, residuo.getCodR());
            stmt.executeUpdate();
            return true;
        }catch(SQLException ex){
            throw new SQLException("Erro ao remover o residuo. Origem: " +ex.getMessage());
        }finally{
            try{stmt.close();}catch(Exception ex){
            	System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage()); throw ex;};
            try{con.close();;}catch(Exception ex){
            	System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage()); throw ex;};                
        }
    }
    
    public boolean removerResiduo(Residuo residuo) throws SQLException, ClassNotFoundException, Exception{
        Connection con = null;
        PreparedStatement stmt = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtRemoverR);
            stmt.setInt(1, residuo.getCodR());
            stmt.executeUpdate();
            return true;
        }catch(SQLException ex){
            throw new SQLException("Erro ao deletar o residuo. Origem: " +ex.getMessage());
        }finally{
            try{stmt.close();}catch(Exception ex){
            	System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage()); throw ex;};
            try{con.close();;}catch(Exception ex){
            	System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage()); throw ex;};                
        }
    }
    
    public List<Residuo> listarResiduos() throws SQLException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Residuo> lista = new ArrayList<Residuo>();
        try {
            con = ConnectionFactory.getConnection();
        } catch (ClassNotFoundException ex) {
            System.out.println("Erro ao listar os residuos");
        }
        stmt = con.prepareStatement(stmtListarR);
        rs = stmt.executeQuery();
        while(rs.next()){
    		Residuo residuo = new Residuo(rs.getInt("codr"), rs.getString("nome"), rs.getString("descricao"), rs.getString("imagem_residuo"));
    		CategoriaDAO catDAO = new CategoriaDAO();
    		Categoria categoria = catDAO.pesquisarCategoria(rs.getInt("categoria_codc")); 
            residuo.setCategoria( 
            		categoria != null ? categoria : new Categoria(0, "S/C", "#555555") 
            		);
    		lista.add(residuo);
        }
        return lista;
    }
    
    public List<Residuo> listarResiduos(Categoria categoria) throws SQLException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Residuo> lista = new ArrayList<Residuo>();
        try {
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtBuscaPelaCat);
            stmt.setInt(1, categoria.getCodC());
            rs = stmt.executeQuery();
            while(rs.next()){
        		Residuo residuo = new Residuo(rs.getInt("codr"), rs.getString("nome"), rs.getString("descricao"), rs.getString("imagem_residuo"));
                CategoriaDAO catDAO = new CategoriaDAO();
                residuo.setCategoria( 
                		catDAO.pesquisarCategoria(rs.getInt("categoria_codc"))
                		);
        		lista.add(residuo);
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("Erro ao listar os residuos");
        }
        
        return lista;
    }
    
    public Residuo pesquisarResiduo(int codR) throws SQLException, Exception { // utilizada na alteração
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Residuo residuo = null;
        try{
        	con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtPesquisaAltR);
            stmt.setInt(1, codR);
            rs = stmt.executeQuery();
            if(rs.next()){
            	residuo = new Residuo(rs.getInt("codr"), rs.getString("nome"), rs.getString("descricao"), rs.getString("imagem_residuo"));
            	CategoriaDAO catDAO = new CategoriaDAO();
                residuo.setCategoria( 
                		catDAO.pesquisarCategoria(rs.getInt("categoria_codc"))
                		);
            }
            return residuo;
        }catch(SQLException ex){
            throw new SQLException("Erro ao pesquisar o cliente. Origem=" +ex.getMessage());
        } catch (Exception e){
        	throw new Exception(e.getMessage());
        }finally{
            try{rs.close();}catch(Exception ex){System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());};
            try{stmt.close();}catch(Exception ex){System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());};
            try{con.close();;}catch(Exception ex){System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());};                
        }
    }
     
    public List<Residuo> buscarResiduo(Residuo residuo) throws SQLException, ClassNotFoundException, Exception { // --> campo de busca nome
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Residuo> lista = new ArrayList<Residuo>();
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtBuscarR);
            stmt.setString(1, "%"+ residuo.getNome() +"%");
            rs = stmt.executeQuery();
            while(rs.next()){
            	residuo = new Residuo(rs.getInt("codr"), rs.getString("nome"), rs.getString("descricao"), rs.getString("imagem_residuo"));
            	CategoriaDAO catDAO = new CategoriaDAO();
                residuo.setCategoria( 
                		catDAO.pesquisarCategoria(rs.getInt("categoria_codc"))
                		);
            	lista.add(residuo);
            }
            return lista;
        }catch(SQLException ex){
            throw new SQLException("Erro ao buscar cliente. Origem: " + ex.getMessage());
        }finally{
            try{rs.close();}catch(Exception ex){System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());};
            try{stmt.close();}catch(Exception ex){System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());};
            try{con.close();;}catch(Exception ex){System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());};
        }
    }
    
    public List<Residuo> buscaExataResiduo(Residuo residuo) throws ClassNotFoundException, SQLException, Exception { 
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Residuo> lista = new ArrayList<Residuo>();
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtBuscaExata);
            stmt.setString(1, residuo.getNome());
            rs = stmt.executeQuery();
            while(rs.next()){
            	residuo = new Residuo(rs.getInt("codr"), rs.getString("nome"), rs.getString("descricao"), rs.getString("imagem_residuo"));
            	CategoriaDAO catDAO = new CategoriaDAO();
                residuo.setCategoria( 
                		catDAO.pesquisarCategoria(rs.getInt("categoria_codc"))
                		);
            	lista.add(residuo);
            }
            return lista;
        }catch(SQLException ex){
            throw new SQLException("Erro ao buscar resíduo. Origem: "+ex.getMessage());
        }finally{
            try{rs.close();}catch(Exception ex){
            	throw new Exception("Erro ao fechar result set. Ex="+ex.getMessage());
            	};
            try{stmt.close();}catch(Exception ex){
            	throw new Exception("Erro ao fechar stmt. Ex="+ex.getMessage());
            	};
            try{con.close();;}catch(Exception ex){
            	throw new Exception("Erro ao fechar conexão. Ex="+ex.getMessage());
            	};
        }
    }
    
}
