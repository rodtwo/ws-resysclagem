package resysclagem.ws.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import com.google.gson.Gson;

import resysclagem.ws.classes.PontoColeta;
import resysclagem.ws.classes.Residuo;

public class PontoColetaDAO {
	private final String stmtInserirPC = "INSERT INTO pontocoleta (nome, imagem, latitude, longitude, "
			+ "endereco, descricao, telefone, email, hora_atendimento, msg_requisicao, pendente) "
			+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	private final String stmtInserirPcoRes = "INSERT INTO pontocoleta_has_residuo (pontocoleta_codpc, residuo_codr) "
			+ "VALUES (?, ?)";
	private final String stmtLimparPcoRes = "DELETE FROM pontocoleta_has_residuo WHERE pontocoleta_codpc=? ";
	
	private final String stmtListarPC = "SELECT * FROM pontocoleta WHERE deletado=false AND pendente = false;";
	
	private final String stmtListarPCsPendentes = "SELECT * FROM pontocoleta WHERE deletado=false AND pendente = true;";
	
	private final String stmtBuscarPcoRes = "SELECT * FROM pontocoleta_has_residuo WHERE pontocoleta_codpc = ?";
	
	private final String stmtAlterarPC = "UPDATE pontocoleta SET nome=?, imagem=?, latitude=?, longitude=?, "
			+ "endereco=?, descricao=?, telefone=?, email=?, hora_atendimento=?, msg_requisicao=?, pendente=? "
			+ "WHERE codpc=?";
    
	private final String stmtRemoverPC = "UPDATE pontocoleta SET deletado=true WHERE codpc=?";
    
	private final String stmtBuscarPC = "SELECT * FROM pontocoleta WHERE nome LIKE ? AND deletado=false ORDER BY nome";
    
	private final String stmtBuscaCodPC = "SELECT * FROM pontocoleta WHERE codpc=? AND deletado=false";
    
	private final String stmtBuscaNomePC = "SELECT * FROM pontocoleta WHERE nome=? AND deletado=false";
    
	private final String stmtBuscaPCcomListResiduo = "SELECT * FROM pontocoleta_has_residuo WHERE residuo_codr=?";
    
    public PontoColeta inserirPCO(PontoColeta pco) throws SQLException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        
        String latitude = String.format(Locale.ROOT, "%.9f", pco.getLatitude());
        String longitude = String.format(Locale.ROOT, "%.9f", pco.getLongitude());
        try{
            con = ConnectionFactory.getConnection();
            con.setAutoCommit(false);
            stmt = con.prepareStatement(stmtInserirPC, PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, pco.getNome());
            stmt.setString(2, pco.getImagem());
            stmt.setString(3, latitude);
            stmt.setString(4, longitude);
            stmt.setString(5, pco.getEndereco());
            stmt.setString(6, pco.getDescricao());
            stmt.setString(7, pco.getTelefone());
            stmt.setString(8, pco.getEmail());
            stmt.setString(9, pco.getHoraAtendimentoBD());
            stmt.setString(10, pco.getMsgRequisicao());
            stmt.setBoolean(11, pco.isPendente());
            stmt.executeUpdate();
            con.commit();
            
            ResultSet rs = stmt.getGeneratedKeys();  
            if(rs.next())
            	pco.setCodPC(rs.getInt(1));
            rs.close();
            boolean pcoHasRes = this.inserirEmPcoHasResiduo(pco);
            
            if(pcoHasRes){
            	return pco;
            }
            return null;
        } catch (SQLException ex) {
            try{con.rollback();}
            catch(SQLException ex1) {
            	throw new SQLException("Erro ao tentar rollback. Ex="+ex1.getMessage());
            	};
            System.err.println(new Gson().toJson(pco));
            ex.printStackTrace();
            throw new SQLException("Erro ao inserir um ponto de coleta. Origem: " + ex.getMessage());
        } catch (Exception e) {
        	try{con.rollback();}
            catch(SQLException ex1) {
            	throw new SQLException("Erro ao tentar rollback. Ex="+ex1.getMessage());
            	};
            System.err.println(new Gson().toJson(pco));
            e.printStackTrace();
            throw new Exception("Erro ao inserir um ponto de coleta. Origem: " + e.getMessage());
        } finally{
            try{ stmt.close(); con.close();  
            }catch(Exception ex){
            	throw new Exception("Erro ao fechar stmt. Ex="+ex.getMessage());
            	};
            try{con.close();;}catch(Exception ex){
            	throw new Exception("Erro ao fechar conexão. Ex="+ex.getMessage());
            	};
        }
    }
    
    public boolean inserirEmPcoHasResiduo(PontoColeta pco) throws Exception {
    	Connection con = null;
    	PreparedStatement stmt = null;
    	try {
    		con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtInserirPcoRes, PreparedStatement.RETURN_GENERATED_KEYS);
            int i = 0;
            if(pco.getResiduos() != null){
	            for (Residuo res : pco.getResiduos()) {
	            	stmt.setInt(1, pco.getCodPC());
	            	stmt.setInt(2, res.getCodR());
	            	stmt.addBatch();
	                i++;
	                if (i % 100 == 0 || i == pco.getResiduos().size()) {
	                    stmt.executeBatch(); // Execute every 100 items.
	                }
	            }
            }
            return true;
    	} catch (SQLException ex) {
    		
    		ex.printStackTrace();
            throw new SQLException("Erro ao inserir um ponto de coleta. Origem: " + ex.getMessage());
        } catch (Exception e) {
			e.printStackTrace();
		} finally{
            try{ stmt.close(); stmt.close();  
            }catch(Exception ex){
            	throw new Exception("Erro ao fechar stmt. Ex="+ex.getMessage());
            	};
            try{con.close();;}catch(Exception ex){
            	throw new Exception("Erro ao fechar conexão. Ex="+ex.getMessage());
            	};
        }
    	return false;
    }
    
    public boolean limparPcoHasResiduo(PontoColeta pco) throws Exception {
    	Connection con = null;
    	PreparedStatement stmt = null;
    	try {
    		con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtLimparPcoRes);
	        stmt.setInt(1, pco.getCodPC());
	        stmt.executeUpdate();
            return true;
    	} catch (SQLException ex) {
    		ex.printStackTrace();
            throw new SQLException("Erro ao limpar tabela. Origem: " + ex.getMessage());
        } catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Erro ao limpar tabela. Origem: " + e.getMessage());
		} finally{
            try{ stmt.close(); stmt.close();  
            }catch(Exception ex){
            	throw new Exception("Erro ao fechar stmt. Ex="+ex.getMessage());
            	};
            try{con.close();;}catch(Exception ex){
            	throw new Exception("Erro ao fechar conexão. Ex="+ex.getMessage());
            	};
        }
    }
    
    public PontoColeta alterarPCO(PontoColeta pco) throws SQLException, ClassNotFoundException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        String latitude = String.format(Locale.ROOT, "%.9f", pco.getLatitude());
        String longitude = String.format(Locale.ROOT, "%.9f", pco.getLongitude());
        try{
            con = ConnectionFactory.getConnection();
            con.setAutoCommit(false);
            stmt = con.prepareStatement(stmtAlterarPC);
            stmt.setString(1, pco.getNome());
            stmt.setString(2, pco.getImagem());
            stmt.setString(3, latitude);
            stmt.setString(4, longitude);
            stmt.setString(5, pco.getEndereco());
            stmt.setString(6, pco.getDescricao());
            stmt.setString(7, pco.getTelefone());
            stmt.setString(8, pco.getEmail());
            stmt.setString(9, pco.getHoraAtendimentoBD());
            stmt.setString(10, pco.getMsgRequisicao());
            stmt.setBoolean(11, pco.isPendente());
            
            stmt.setInt(12, pco.getCodPC());
            stmt.executeUpdate();
            con.commit();
            
            if(this.limparPcoHasResiduo(pco)) {
            	if(this.inserirEmPcoHasResiduo(pco)) {
            		return pco;
            	}
            }
            return null;
        }catch(SQLException ex){
            throw new SQLException("Erro ao alterar o ponto de coleta. Origem: " + ex.getMessage());
        }finally{
            try{stmt.close();}catch(Exception ex){
            	System.out.println("Erro ao fechar stmt. Ex = " +ex.getMessage()); 
            	throw ex;
            	};
            try{con.close();;}catch(Exception ex){
            	System.out.println("Erro ao fechar conexão. Ex= "+ex.getMessage()); 
            	throw ex;
            	};
        }
    }
    
    public boolean removerPCO(PontoColeta pco) throws SQLException, ClassNotFoundException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtRemoverPC);
            stmt.setInt(1, pco.getCodPC());
            stmt.executeUpdate();
            return true;
        }catch(SQLException ex){
            throw new SQLException("Erro ao deletar o ponto. Origem: " +ex.getMessage());
        }finally{
            try{stmt.close();}catch(Exception ex){
            	System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage()); 
            	throw ex;
            	};
            try{con.close();;}catch(Exception ex){
            	System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage()); 
            	throw ex;
            	};                
        }
    }
    
    //read
    public List<PontoColeta> listarTodosPCOs(boolean isPendente) throws SQLException, ClassNotFoundException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<PontoColeta> lista = new ArrayList<PontoColeta>();
        String stmtListar = isPendente ? stmtListarPCsPendentes : stmtListarPC;
        try {
            con = ConnectionFactory.getConnection();
	        stmt = con.prepareStatement(stmtListar);
	        rs = stmt.executeQuery();
	                
	        while(rs.next()){
	        	ResiduoDAO resDAO = new ResiduoDAO();
		        List<Residuo> residuos = new ArrayList<Residuo>();
		        
		        for( Integer i : this.buscarCodResiduo_PcoHasRes(rs.getInt("codpc")) ) { 
		        	residuos.add(resDAO.pesquisarResiduo(i));
		        }
	        	
	        	PontoColeta pco = new PontoColeta(
	    				rs.getInt("codpc"), 
	    				rs.getString("nome"), 
	    				rs.getString("imagem"),
	    				rs.getString("endereco"),
	    				rs.getString("descricao"),
	    				rs.getString("telefone"),
	    				rs.getString("email"),
	    				rs.getString("hora_atendimento"),
	    				rs.getString("latitude"),
	    				rs.getString("longitude"),
	    				residuos,
	    				rs.getString("msg_requisicao"),
	    				rs.getBoolean("pendente")
	    				);
	            lista.add(pco);
	        }
	        return lista;
        } catch (SQLException e){
        	throw new SQLException(e.getMessage());
        } catch (ClassNotFoundException ex) {
            throw new Exception("Erro ao listar os pontos: " + ex.getMessage());
	    } catch (Exception e) {
	    	e.printStackTrace();
	        throw new Exception(e.getMessage());
	    }
    }

    public List<Residuo> buscarResiduo_PcoHasRes(int codPC) throws SQLException, Exception {
    	try{
	    	Connection con = ConnectionFactory.getConnection();
	    	PreparedStatement stmtPcoRes = null;
	        ResultSet rsPcoRes = null;
	        
	        stmtPcoRes = con.prepareStatement(stmtBuscarPcoRes);
	        stmtPcoRes.setInt(1, codPC);
	        rsPcoRes = stmtPcoRes.executeQuery();
	        
	        List<Residuo> codsResiduos = new ArrayList<Residuo>();
	        ResiduoDAO residuoDAO = new ResiduoDAO();
	        while(rsPcoRes.next()){
	        	Residuo res = residuoDAO.pesquisarResiduo(rsPcoRes.getInt("residuo_codr"));
	        	codsResiduos.add(res);
	        }
	        return codsResiduos;
    	} catch(SQLException sqle){
    		sqle.printStackTrace();
    		throw sqle;
    	} catch(Exception e){
    		e.printStackTrace();
    		throw e;
    	}
    }
    
    public List<Integer> buscarCodResiduo_PcoHasRes(int codPC) throws SQLException, Exception {
    	try{
	    	Connection con = ConnectionFactory.getConnection();
	    	PreparedStatement stmtPcoRes = null;
	        ResultSet rsPcoRes = null;
	        stmtPcoRes = con.prepareStatement(stmtBuscarPcoRes);
	        stmtPcoRes.setInt(1, codPC);
	        rsPcoRes = stmtPcoRes.executeQuery();
	        
	        List<Integer> codsResiduos = new ArrayList<Integer>(); 
	        while(rsPcoRes.next()){
	        	codsResiduos.add(rsPcoRes.getInt("residuo_codr"));
	        }
	        return codsResiduos;
    	} catch(SQLException sqle){
    		sqle.printStackTrace();
    		throw sqle;
    	} catch(Exception e){
    		e.printStackTrace();
    		throw e;
    	}
    }
    
    public PontoColeta pesquisarPCO(int codPC) throws SQLException, Exception { // utilizada na alteração(?)
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        PontoColeta pco = null;
        try{
        	con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtBuscaCodPC);
            stmt.setInt(1, codPC);
            rs = stmt.executeQuery();
            if(rs.next()){
            	List<Residuo> residuos = new ArrayList<Residuo>(); 
    	        for( Residuo res : this.buscarResiduo_PcoHasRes(rs.getInt("codpc")) ){
    	        	residuos.add(res);
    	        }
            	
	    		pco = new PontoColeta(
	    				rs.getInt("codpc"), 
	    				rs.getString("nome"), 
	    				rs.getString("imagem"),
	    				rs.getString("endereco"),
	    				rs.getString("descricao"),
	    				rs.getString("telefone"),
	    				rs.getString("email"),
	    				rs.getString("hora_atendimento"),
	    				rs.getString("latitude"),
	    				rs.getString("longitude"),
	    				residuos,
	    				rs.getString("msg_requisicao"),
	    				rs.getBoolean("pendente")
	    				);
            }
            return pco;
        }catch(SQLException ex){
            throw new SQLException("Erro ao pesquisar o ponto de coleta. Origem: " +ex.getMessage());
        } catch (Exception e){
        	throw new Exception(e.getMessage());
        }finally{
            try{rs.close();}catch(Exception ex){
            	System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            	throw ex;
            	};
            try{stmt.close();}catch(Exception ex){
            	System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            	throw ex;
            	};
            try{con.close();;}catch(Exception ex){
            	System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            	throw ex;
            	};                
        }
    }
     
    public List<PontoColeta> buscarPCO(PontoColeta pco) throws Exception{ // --> campo de busca nome
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<PontoColeta> lista = new ArrayList<PontoColeta>();
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtBuscarPC);
            stmt.setString(1, "%"+ pco.getNome() +"%");
            rs = stmt.executeQuery();
            while(rs.next()){
               	List<Residuo> residuos = new ArrayList<Residuo>(); 
    	        for( Residuo res : this.buscarResiduo_PcoHasRes(rs.getInt("codpc")) ){
    	        	residuos.add(res);
    	        }
            	
            	PontoColeta ponto = new PontoColeta(
	    				rs.getInt("codpc"), 
	    				rs.getString("nome"), 
	    				rs.getString("imagem"),
	    				rs.getString("endereco"),
	    				rs.getString("descricao"),
	    				rs.getString("telefone"),
	    				rs.getString("email"),
	    				rs.getString("hora_atendimento"),
	    				rs.getString("latitude"),
	    				rs.getString("longitude"),
	    				residuos,
	    				rs.getString("msgRequisicao"),
	    				rs.getBoolean("pendente")
            			);
            	lista.add(ponto);
            }
            return lista;
        }catch(SQLException ex){
            throw new SQLException("Erro ao buscar ponto. Origem: " + ex.getMessage());
        } catch (Exception e) {
        	throw new Exception("Erro ao buscar ponto. Origem: " + e.getMessage());
		}finally{
            try{rs.close();}catch(Exception ex){System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());};
            try{stmt.close();}catch(Exception ex){System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());};
            try{con.close();;}catch(Exception ex){System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());};
        }
    }
    
    public List<PontoColeta> buscaExataNomePCO(PontoColeta pco) throws Exception { 
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<PontoColeta> lista = new ArrayList<PontoColeta>();
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtBuscaNomePC);
            stmt.setString(1, pco.getNome());
            rs = stmt.executeQuery();
            while(rs.next()){
               	List<Residuo> residuos = new ArrayList<Residuo>(); 
    	        for( Residuo res : this.buscarResiduo_PcoHasRes(rs.getInt("codpc")) ){
    	        	residuos.add(res);
    	        }
            	
            	PontoColeta ponto = new PontoColeta(
	    				rs.getInt("codpc"), 
	    				rs.getString("nome"), 
	    				rs.getString("imagem"),
	    				rs.getString("endereco"),
	    				rs.getString("descricao"),
	    				rs.getString("telefone"),
	    				rs.getString("email"),
	    				rs.getString("hora_atendimento"),
	    				rs.getString("latitude"),
	    				rs.getString("longitude"),
	    				residuos,
	    				rs.getString("msgRequisicao"),
	    				rs.getBoolean("pendente")
            			);
            	lista.add(ponto);
            }
            return lista;
        }catch(SQLException ex){
        	ex.printStackTrace();
            throw new SQLException("Erro ao buscar PCO. Origem: "+ex.getMessage());
        } catch (Exception e) {
        	e.printStackTrace();
        	throw new Exception("Erro ao buscar PCO. Origem: "+e.getMessage());
		}finally{
            try{rs.close();}catch(Exception ex){System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());};
            try{stmt.close();}catch(Exception ex){System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());};
            try{con.close();;}catch(Exception ex){System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());};
        }
    }
    
    public List<PontoColeta> buscarPCOsQueTenhamResiduos(List<Residuo> listResiduos) throws Exception {
        List<PontoColeta> listaPCOs = new ArrayList<PontoColeta>();
        //for avançado: pra cada residuo na lista, quais os pontos relacionados a ele
        for(Residuo residuo : listResiduos){
    		Connection con = null;
            PreparedStatement stmt = null;
            ResultSet rs = null;
            try {
                con = ConnectionFactory.getConnection();
                stmt = con.prepareStatement(stmtBuscaPCcomListResiduo);
                stmt.setInt(1, residuo.getCodR());
                rs = stmt.executeQuery();
                while(rs.next()){
                	PontoColeta ponto = new PontoColetaDAO().pesquisarPCO(
                			rs.getInt("pontoColeta_codpc")
                			);
                	if(!listaPCOs.contains(ponto)){
                		listaPCOs.add(ponto);
                	}
                }
            }catch(SQLException ex){
                throw new SQLException("Erro ao buscar ponto. Origem: " + ex.getMessage());
            } catch (Exception e) {
            	throw new Exception("Erro ao buscar ponto. Origem: " + e.getMessage());
    		}finally{
                try{rs.close();}catch(Exception ex){System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());};
                try{stmt.close();}catch(Exception ex){System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());};
                try{con.close();;}catch(Exception ex){System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());};
            }
        }
        
        for (Iterator<PontoColeta> i = listaPCOs.iterator(); i.hasNext();) {
        	  PontoColeta pco = i.next();
        	  if(pco != null
        			  && pco.getResiduos() != null
        			  && listResiduos != null
        			  && !pco.getResiduos().containsAll(listResiduos)
        			  ){
        	    i.remove();
        	  }
        }
        
        return listaPCOs;
        
    }
    
    
}
