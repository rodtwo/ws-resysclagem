package resysclagem.ws.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import resysclagem.ws.classes.Administrador;
import resysclagem.ws.classes.Residuo;
import resysclagem.ws.classes.Usuario;

public class AdministradorDAO {
	private final String stmtConsultarAdmin = "SELECT * FROM administrador WHERE coda=? OR email=?";
	private final String stmtConsultarAdminMaster = "SELECT * FROM administrador WHERE master = true";
	private final String stmtLoginAdmin = "SELECT * FROM administrador WHERE email=? AND senha=?";
	private final String stmtListar = "SELECT * FROM administrador";
	private final String stmtInserirA = "INSERT INTO administrador (nome, email, senha) VALUES (?,?,?)";
    private final String stmtAlterarA = "UPDATE administrador SET nome=?, email=?, senha=? WHERE coda=?";
    private final String stmtRemoverA = "DELETE FROM administrador WHERE coda=?";
    
    public Administrador consultarAdmin(Administrador admin) throws SQLException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        SQLException sqle = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtConsultarAdmin);
            stmt.setInt(1, admin.getCodA());
            stmt.setString(2, admin.getEmail());
            admin = null;
            rs = stmt.executeQuery();
            if(rs.next())
            	admin = new Administrador(rs.getInt("coda"), rs.getString("nome"), rs.getString("email") /* não vazar senha */);
            return admin;
        }catch(SQLException ex){
        	ex.printStackTrace();
        	sqle = ex;
        	return null;        	
        	/* Se der um 'Throw' dentro do catch e tiver o mesmo dentro de um finally, o primeiro será ignorado.
        	 * Deixar throws dentro apenas do catch ou do throw!	
        	 */
            // throw new SQLException("Erro ao fazer login. Origem="+ex.getMessage());
        }finally{
        	if (sqle != null) {
        		System.err.println("Erro ao fazer login. Origem: "+sqle.getMessage());
        		throw sqle;
        	}
            try {rs.close();}catch(Exception ex){
            	throw new Exception("Erro ao fechar result set. Ex="+ex.getMessage());
            	};
            try {stmt.close();}catch(Exception ex){
            	throw new Exception("Erro ao fechar stmt. Ex="+ex.getMessage());
            	};
            try {con.close();;}catch(Exception ex){
            	throw new Exception("Erro ao fechar conexão. Ex="+ex.getMessage());
            	};
        }
    }
    
    public Administrador consultarAdminMaster() throws SQLException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Administrador admin = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtConsultarAdminMaster);
            rs = stmt.executeQuery();
            if(rs.next())
            	admin = new Administrador(rs.getInt("coda"), rs.getString("nome"), rs.getString("email"), null /* não vazar senha */);
            return admin;
        }catch(SQLException ex){
            throw new SQLException("Erro ao fazer login. Origem="+ex.getMessage());
        }finally{
            try{rs.close();}catch(Exception ex){
            	throw new Exception("Erro ao fechar result set. Ex="+ex.getMessage());
            	};
            try{stmt.close();}catch(Exception ex){
            	throw new Exception("Erro ao fechar stmt. Ex="+ex.getMessage());
            	};
            try{con.close();;}catch(Exception ex){
            	throw new Exception("Erro ao fechar conexão. Ex="+ex.getMessage());
            	};
        }
    }

    public Administrador fazerLogin (Administrador admin) throws SQLException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try{
            con = ConnectionFactory.getConnection();
	        stmt = con.prepareStatement(stmtLoginAdmin);
	        stmt.setString(1, admin.getEmail());
	        stmt.setString(2, admin.getSenha());
            admin = null;
            rs = stmt.executeQuery();
            if(rs.next())
            	admin = new Administrador(rs.getInt("coda"), rs.getString("nome"), rs.getString("email") /*rs.getString("senha")*/);
            return admin;
        }catch(SQLException ex){
            throw new SQLException("Erro ao fazer login. Origem="+ex.getMessage());
        }finally{
            try{rs.close();}catch(Exception ex){
            	throw new Exception("Erro ao fechar result set. Ex="+ex.getMessage());
            	};
            try{stmt.close();}catch(Exception ex){
            	throw new Exception("Erro ao fechar stmt. Ex="+ex.getMessage());
            	};
            try{con.close();;}catch(Exception ex){
            	throw new Exception("Erro ao fechar conexão. Ex="+ex.getMessage());
            	};
        }
    }
    
    
    public Administrador inserirAdmin(Administrador admin) throws SQLException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        Administrador retornado = null;
        try{
            con = ConnectionFactory.getConnection();
            con.setAutoCommit(false);
            stmt = con.prepareStatement(stmtInserirA,PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, admin.getNome());
            stmt.setString(2, admin.getEmail());
            stmt.setString(3, admin.getSenha());
            stmt.executeUpdate();
            con.commit();
            
            ResultSet rs = stmt.getGeneratedKeys();  
            if(rs.next())
            	admin.setCodA(rs.getInt(1));
            	admin.setSenha(null);
            	retornado = admin;
            rs.close();
            
            return retornado;
        } catch (SQLException ex) {
        	ex.printStackTrace();
            try{
            	con.rollback();
            }catch(SQLException ex1){System.out.println("Erro ao tentar rollback. Ex="+ex1.getMessage());};
            throw new SQLException("Erro ao inserir um administrador no banco de dados. Origem="+ex.getMessage());
        } catch (Exception e) {
        	e.printStackTrace();
            try{
            	con.rollback();}
            catch(SQLException ex1){System.out.println("Erro ao tentar rollback. Ex="+ex1.getMessage());};
            throw new Exception("Erro ao inserir um administrador no banco de dados. Origem="+e.getMessage());
        } finally{
            try{stmt.close();}catch(Exception ex){System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());};
            try{con.close();;}catch(Exception ex){System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());};
        }
    }
    
    public boolean alterarAdmin(Administrador admin) throws SQLException, ClassNotFoundException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtAlterarA);
            stmt.setString(1, admin.getNome());
            stmt.setString(2, admin.getEmail());
            stmt.setString(3, admin.getSenha());
            stmt.setInt(4, admin.getCodA());
            stmt.executeUpdate();
            
            return true;
        }catch(SQLException ex){
            throw new RuntimeException("Erro ao alterar o administrador. Origem = " +ex.getMessage());
        }finally{
            try{stmt.close();}catch(Exception ex){System.out.println("Erro ao fechar stmt. Ex = " +ex.getMessage());};
            try{con.close();;}catch(Exception ex){System.out.println("Erro ao fechar conexão. Ex= "+ex.getMessage());};
        }
    }
    
    public boolean removerAdmin(Administrador admin) throws ClassNotFoundException{
        Connection con = null;
        PreparedStatement stmt = null;
        try{
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtRemoverA);
            stmt.setInt(1, admin.getCodA());
            stmt.executeUpdate();
            
            return true;
        }catch(SQLException ex){
            throw new RuntimeException("Erro ao deletar o administrador. Origem=" +ex.getMessage());
        }finally{
            try{stmt.close();}catch(Exception ex){System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());};
            try{con.close();;}catch(Exception ex){System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());};                
        }
    }

	public List<Administrador> listarTodos()  throws SQLException, Exception {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Administrador> lista = new ArrayList<Administrador>();
        try {
            con = ConnectionFactory.getConnection();
            stmt = con.prepareStatement(stmtListar);
            rs = stmt.executeQuery();
            
            while(rs.next()){
        		Administrador admin = new Administrador(rs.getInt("coda"), rs.getString("nome"), rs.getString("email"));
                lista.add(admin);
            }
            return lista;
            
        } catch (SQLException sqle) {
            throw sqle;
        } finally {
        	try{
        		rs.close();
        		stmt.close();
        		con.close();
        	} catch (Exception e) { throw e; }
        }

    }


}
