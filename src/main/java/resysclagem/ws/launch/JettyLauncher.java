package resysclagem.ws.launch;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Inet4Address;
import java.util.Collections;
import java.util.EnumSet;

import javax.servlet.DispatcherType;

import org.apache.commons.io.output.TeeOutputStream;
import org.eclipse.jetty.http.HttpVersion;
import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.HashLoginService;
import org.eclipse.jetty.security.SecurityHandler;
import org.eclipse.jetty.security.authentication.BasicAuthenticator;
import org.eclipse.jetty.server.ConnectionFactory;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.SslConnectionFactory;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.eclipse.jetty.util.security.Constraint;
import org.eclipse.jetty.util.security.Password;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

import resysclagem.ws.PropertySource;
import resysclagem.ws.config.jetty.CORSHandler;
import resysclagem.ws.config.jobs.FilesCleanup;
import resysclagem.ws.config.jobs.ServerURLFilesFixer;
import resysclagem.ws.token.Resystoken;

public final class JettyLauncher {

	private static Server 		appServer;
	public final static String 	buildPath;
	public static Boolean 		prod = true;
	public static String 		protocol = "https";
	public static String 		host = "localhost";
	public static int 			port = 8080;
	private static String 		staticContent = "";
	public static String 		services = "";
	public static String 		files = "";
	private static Boolean 		secure = true;
	public  static Resystoken	tokenClass;
	private static String 		realm = "";
	private static String 		logToFile = "both";
	private final static String 	dbUrl;
    private final static String 	dbUsuario;
    private final static String 	dbSenha;
	
    /* 
     * Static content - inicializar propriedades, etc
     * 
     */
    static {
    	
    	// path relativo
    	buildPath = new File("").getAbsolutePath();
		System.out.println("* Build path: " + buildPath);
		
		// prod : dev properties
		prod = Boolean.valueOf(PropertySource.props.getProperty("resysclagem.launch.prod"));
		String sufixoProd = "";		
		if(prod){
			System.out.println("* Production mode ON (Prod mode)");
			sufixoProd = ".prod";
		} else {
			System.out.println("* Not prod application (Dev mode)");
		}
	
		services = PropertySource.props.getProperty("resysclagem.launch.services");
		files = PropertySource.props.getProperty("resysclagem.file");
		staticContent = PropertySource.props.getProperty("resysclagem.webapps");
		secure = Boolean.valueOf(PropertySource.props.getProperty("resysclagem.launch.security.basicauth"));
		realm = PropertySource.props.getProperty("resysclagem.realm.props");		
		logToFile = PropertySource.props.getProperty("resysclagem.launch.logtofile");
		
		try {
			tokenClass = new Resystoken(
										PropertySource.props.getProperty("resysclagem.launch.token.key"),
										Boolean.valueOf(PropertySource.props.getProperty("resysclagem.launch.security.requiretoken"))
									   );
		} catch (Exception e) {
			System.err.println("Error initializing " + Resystoken.class.getName());
			e.printStackTrace();
		}
		
		// DB
		dbUrl = PropertySource.props.getProperty("resysclagem.bd"+ sufixoProd +".url");
        dbUsuario = PropertySource.props.getProperty("resysclagem.bd"+ sufixoProd +".usuario");
        dbSenha = PropertySource.props.getProperty("resysclagem.bd"+ sufixoProd +".senha"); // pensar em em maneira de encriptar

        // ws - próprio endpoint (usado em URLs pra imgs, etc)
        protocol = PropertySource.props.getProperty("resysclagem.launch"+ sufixoProd +".protocol");
        host = PropertySource.props.getProperty("resysclagem.launch"+ sufixoProd +".host");
       
		try {
			port = Integer.valueOf(PropertySource.props.getProperty("resysclagem.launch"+ sufixoProd +".port"));
		} catch(NumberFormatException nfe){
			String strPort = PropertySource.props.getProperty("resysclagem.launch"+ sufixoProd +".port");
			if(strPort.equalsIgnoreCase("heroku")){
				 port = Integer.valueOf(System.getenv("PORT"));
			}
		}
		
    }
    
	public static void main(String[] args) throws Exception {
		launch();
	}
	
	/**
	 * Initializing the server
	 * 
	 * @throws IOException
	 * @throws Exception
	 */
	public static void launch() throws IOException, Exception{
	
		resysclagem.ws.dao.ConnectionFactory.setParameters(dbUrl, dbUsuario, dbSenha);
		
		appServer = new Server(port);
		HandlerList hList = new HandlerList();
        
        if(protocol.equalsIgnoreCase("https")){
        	ssl(appServer);
        }
		
		//static content
		ResourceHandler resourceHandler= new ResourceHandler();
        resourceHandler.setResourceBase(staticContent);		// webapps
        resourceHandler.setDirectoriesListed(true);
        resourceHandler.setWelcomeFiles(new String[]{"index.html"});
        ContextHandler webContext = new ContextHandler("/");  /* the server uri path */
        webContext.setHandler(resourceHandler);
        
        //webservices
		ResourceConfig config = new ResourceConfig();
		config.register(MultiPartFeature.class);
		config.packages("resysclagem.ws.services");
		config.register(JacksonFeature.class);
		ServletHolder servlet = new ServletHolder(new ServletContainer(config));
		ServletContextHandler svrContext = new ServletContextHandler(appServer, services);	//context path, services == /server
		svrContext.addServlet(servlet, "/*"); //server/endpoints
		svrContext.setInitParameter("jersey.config.server.provider.packages", "com.jersey.jaxb,com.fasterxml.jackson.jaxrs.json");
		svrContext.setInitParameter("com.sun.jersey.api.json.POJOMappingFeature", "true");
		/*
		 * TODO aqui: posteriormente, autorizar apenas o domínio do módulo WEB a fazer alterações
		 */
		FilterHolder holder = new FilterHolder(new CrossOriginFilter());
		holder.setInitParameter("allowedMethods", "GET,POST,PUT,DELETE,HEAD,OPTIONS");
		//holder.setInitParameter("allowedOrigins", "http://localhost:4200");
		holder.setInitParameter("allowedHeaders", "Content-Type, Accept, X-Requested-With");
		svrContext.addFilter(holder, "/*", EnumSet.of(DispatcherType.REQUEST));
		
		// Does it solve CORS * auth issue?
		//svrContext.setHandler(new CORSHandler());
		
		hList.addHandler(webContext); // index
		hList.addHandler(svrContext);
		
		
		if (secure){
			/*
			 *  basic authentication - ver resysclagem.realm.props
			 */
			ConstraintSecurityHandler security = secureAppServer(appServer, hList);
			security.setHandler(hList);
			appServer.setHandler(security);
		} else {
			/* 
			 * unsecure
			 */
			System.out.println("* Warn: services and static content are not secured by authentication");
			appServer.setHandler(hList);
		}

		try {
			/*
			 * logging / output
			 */
			if(!logToFile.equalsIgnoreCase("both")) {
				boolean boolLog = Boolean.valueOf(logToFile);
				if(boolLog){        	
	        		System.out.println("* From here, output will be printed on log file instead of console.");
	        		PrintStream out = new PrintStream(new FileOutputStream("launchLog.txt"));
	        		System.setOut(out);
	        	} else {
	        		System.out.println("* Output will be printed on console.");
	        	}
			} else {
				System.out.println("* From here, output will be printed on both log file and console.");
				FileOutputStream fos = new FileOutputStream("launchLog.out");
			    //we will want to print in standard "System.out" and in "file"
			    TeeOutputStream bothOut = new TeeOutputStream(System.out, fos);
			    TeeOutputStream bothErr = new TeeOutputStream(System.err, fos);
			    PrintStream psOut = new PrintStream(bothOut, true); 	//true - auto-flush after println
			    PrintStream psErr = new PrintStream(bothErr, true); 	//true - auto-flush after println
			    System.setOut(psOut);
			    System.setErr(psErr);
			}
			
			// Subscribe on routines
			System.err.println("* Scheduling routines...");
			FilesCleanup.scheduleCleanup(false, dbUrl, dbUsuario, dbSenha);
			ServerURLFilesFixer.scheduleFix(true, dbUrl, dbUsuario, dbSenha, 
											protocol, host +":"+ port, files);
			
			/*
			 * START
			 */
			appServer.start();
			System.out.println("* Up at "+ host +":"+ port);
			appServer.join();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	
	public static boolean isRunning() {
		if (appServer == null){
			return false;
		}
		return appServer.isRunning();
	}

	
	public static boolean ssl(Server appServer) throws Exception {
		  
		HttpConfiguration http_config = new HttpConfiguration();
        SslContextFactory sslContextFactory = new SslContextFactory();
    	
    	port = port + 1;
    	// https://github.com/eclipse/jetty.project/blob/jetty-9.3.x/examples/embedded/src/main/java/org/eclipse/jetty/embedded/LikeJettyXml.java
    	// HTTP Configuration
        http_config.setSecureScheme("https");
        http_config.setSecurePort(port); 	// port 8443
        http_config.setOutputBufferSize(32768);
        http_config.setRequestHeaderSize(8192);
        http_config.setResponseHeaderSize(8192);
        http_config.setSendServerVersion(true);
        http_config.setSendDateHeader(false);
        
        // SSL Context Factory            
        String keystorePath = buildPath + File.separator + "resources" + File.separator + "reskey"; 
        File f = new File(keystorePath);
        if(!f.exists()) { 
            throw new Exception("File doesn't exist at "+ keystorePath);
        }
        sslContextFactory.setKeyStorePath(keystorePath);
        String obf = Password.obfuscate("resysadmin*$");
        sslContextFactory.setKeyStorePassword(obf);
        sslContextFactory.setKeyManagerPassword(obf);
        sslContextFactory.setTrustStorePath("resources" + File.separator + "/reskey");
        sslContextFactory.setTrustStorePassword("resysadmin*$");
        sslContextFactory.setExcludeCipherSuites("SSL_RSA_WITH_DES_CBC_SHA",
                "SSL_DHE_RSA_WITH_DES_CBC_SHA", "SSL_DHE_DSS_WITH_DES_CBC_SHA",
                "SSL_RSA_EXPORT_WITH_RC4_40_MD5",
                "SSL_RSA_EXPORT_WITH_DES40_CBC_SHA",
                "SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA",
                "SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA");
        
        // SSL HTTP Configuration
        HttpConfiguration https_config = new HttpConfiguration(http_config);
        https_config.addCustomizer(new SecureRequestCustomizer());

        // SSL Connector
        ServerConnector sslConnector = new ServerConnector(appServer,
        							   new SslConnectionFactory(sslContextFactory,HttpVersion.HTTP_1_1.asString()),
        							   new HttpConnectionFactory(https_config));
        sslConnector.setPort(port);		// 8443
        appServer.addConnector(sslConnector);
		
        return true;
    }
	
	
	public static ConstraintSecurityHandler secureAppServer(Server appServer, HandlerList hList) throws Exception {
		  
		HashLoginService loginService = new HashLoginService("ResysRealm",
				realm);
		appServer.addBean(loginService);
	
		ConstraintSecurityHandler security = new ConstraintSecurityHandler();
		
		// This constraint requires authentication and in addition that an
		// authenticated user be a member of a given set of roles for
		// authorization purposes.
		Constraint constraint = new Constraint();
		constraint.setName("auth");
		constraint.setAuthenticate(true);
		constraint.setRoles(new String[] { "user", "admin" });
		
		// Binds a dbUrl pattern with the previously created constraint. The roles
		// for this constraing mapping are mined from the Constraint itself
		// although methods exist to declare and bind roles separately as well.
		ConstraintMapping mapping = new ConstraintMapping();
		mapping.setPathSpec("/*");
		mapping.setConstraint(constraint);
		security.setConstraintMappings(Collections.singletonList(mapping));
		security.setAuthenticator(new BasicAuthenticator());	//base64
		security.setLoginService(loginService);
		/*
		HashLoginService login = new HashLoginService();
		login.setName("Test Realm");
		login.setConfig("./resources/realm.properties");
		login.setHotReload(false);
		server.addBean(login);
		*/
		
		// setHander secure()
		
		security.setHandler(hList);
			
	    return security;
	}
	
}
