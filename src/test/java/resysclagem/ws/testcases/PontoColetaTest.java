package resysclagem.ws.testcases;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.gson.Gson;

import junit.framework.TestCase;
import resysclagem.ws.classes.PontoColeta;
import resysclagem.ws.classes.Residuo;
import resysclagem.ws.classes.subcls.DiaSemana;
import resysclagem.ws.classes.subcls.HoraAtend;
import resysclagem.ws.utilities.Utility;

public class PontoColetaTest extends TestCase {
	
	private PontoColeta pco;
	
	public PontoColetaTest() throws Exception{
		List<Residuo> residuos = null;
		this.pco = new PontoColeta(0, "Nome", "", "", "", "", "", "2,09:00,16:00 7,09:00,13:00", "0", "0", residuos, "", false);
	}

	
    public void testSetterHoraAtendPC()
    {	
    	List<DiaSemana> dias = new ArrayList<DiaSemana>();
    	for(int i = 2; i <= 6; i++)
    		dias.add(new DiaSemana(i, "09:00", "16:00"));
    	dias.add(new DiaSemana(7, "09:00", "13:00"));
    	
    	HoraAtend horaAtend = new HoraAtend(dias);
    	pco.setHoraAtendimento(horaAtend);
    	
    	Gson gson = new Gson();
    	assertEquals( gson.toJson(horaAtend), gson.toJson(pco.getHoraAtendimento()) );
    }
	
    public void testHoraAtendPC()
    {
    	String hrs = "1,09:00,16:00 7,09:00,13:00";
    	pco.setHoraAtendimento(hrs);
    	assertEquals(hrs, pco.getHoraAtendimentoBD()); //not validpco.getHoraAtendimentoStr();
    }
	
}
