package resysclagem.ws.testcases;

import static org.junit.Assert.*;

import java.awt.Container;

import javax.print.attribute.standard.Media;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Test;

import com.google.gson.Gson;

import junit.framework.AssertionFailedError;
import resysclagem.ws.PropertySource;
import resysclagem.ws.classes.Administrador;
import resysclagem.ws.classes.Request;
import resysclagem.ws.classes.Usuario;
import resysclagem.ws.services.*;
import resysclagem.ws.services.AdminWS.ContaAdmin;

public class TokenTest {

	
	@Test
	public void testGetTokenETestToken() {
		try {
			Usuario usuario = new Usuario("Jão", "21/09/1995", "jao@jao.com", "123456");
	    	// Get Token
			WebTarget webTarget = AppTest.client.target(AppTest.endpoint + "/getToken");
	    	Response response = webTarget
	    						.request(MediaType.APPLICATION_JSON)
	    						.post( Entity.entity(usuario, MediaType.APPLICATION_JSON) );
	    	if (String.valueOf(response.getStatus()).startsWith("2")) {
	    		String token = response.readEntity(String.class);
	    		Request request = new Request(null, usuario.getEmail(), token);
	    		// Test Token
		    	WebTarget webTarget2 = AppTest.client.target(AppTest.endpoint + "/testToken");
		    	Response response2 = webTarget2
		    						 .request(MediaType.APPLICATION_JSON_TYPE)
		    						 .post( Entity.entity(request, MediaType.APPLICATION_JSON) );
		    	if (String.valueOf(response2.getStatus()).startsWith("2")) {
		    		String strBoolean = response2.readEntity(String.class);
		    		System.out.println("Token:" + token);
		    		assertTrue(AppTest.gson.fromJson(strBoolean, Boolean.class));
		    	} else {
		    		System.err.println("HTTP " + response2.getStatus());
		    		fail();
		    	}
	    	} else {
	    		System.err.println("HTTP " + response.getStatus());
	    		fail();
	    	}
    	} catch (AssertionFailedError assertion){
    		System.err.println(assertion.getMessage());
    		throw assertion;
    	} catch (ProcessingException e){
    		throw new ProcessingException("Web service is OFF, turn it on please - " + AppTest.endpoint);
    	}	
	
	}

}
