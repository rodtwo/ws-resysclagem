package resysclagem.ws.testcases;

import java.net.ConnectException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.validation.constraints.AssertTrue;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.digest.DigestUtils;
import org.eclipse.jetty.util.security.Credential.MD5;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.process.internal.RequestScope.Instance;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.google.gson.Gson;

import javassist.expr.Instanceof;
import junit.framework.AssertionFailedError;
import junit.framework.ComparisonFailure;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import resysclagem.ws.PropertySource;
import resysclagem.ws.classes.Usuario;
import resysclagem.ws.launch.JettyLauncher;
import resysclagem.ws.utilities.Utility;


/**
 * Teste unitário pra coisas gerais
 */
public class AppTest extends TestCase {
	
	public static Client client = ClientBuilder.newBuilder()
	        .register(JacksonFeature.class)
	        .build();
	public static String prodStr = Boolean.valueOf(PropertySource.props.getProperty("resysclagem.launch.prod")) ?
													  ".prod" : "";
	public static String endpoint = "http://" + PropertySource.props.getProperty("resysclagem.launch"+ prodStr +".host")
										+ ":" + PropertySource.props.getProperty("resysclagem.launch"+ prodStr +".port")
										 	  + PropertySource.props.getProperty("resysclagem.launch.services");
	public static Gson gson = new Gson();
	
    public void testWsIsOn(){
    	try {
	    	WebTarget webTarget = client.target(endpoint);
	    	Response getResponse =
	    			webTarget.request(MediaType.TEXT_PLAIN_TYPE)
	    	                .get();
	    	if (getResponse.getStatus() == 200){
	    		assertTrue(true);
	    	} else {
	    		System.err.println("HTTP " + getResponse.getStatus());
	    		fail();
	    	}
    	} catch (AssertionFailedError assertion){
    		System.err.println(assertion.getMessage());
    		throw assertion;
    	} catch (ProcessingException e){
    		throw new ProcessingException("Web service is OFF, turn it on please - " + endpoint);
    	}	
    }
    
    //
    //unit tests from here
    //
    public void testPropertiesFile()
    {
    	try {
    		String found = String.valueOf(PropertySource.props.getProperty("resysclagem.prop.test"));
    		assertEquals("found", found);
    	} catch(Exception e) {
    		fail();
    	}
    }
    
    public void testUnexistingPropertiesFile() throws AssertionFailedError
    {
    		assertEquals(
    				String.valueOf(PropertySource.props.getProperty("resysclagem.prop.propertyWhichDoesntExist")),
    				"null"
    				);   
    }

    
    public void testIsMd5()
    {
    	String md5 = DigestUtils.md5Hex("MandioquinhaNoMcDonalds");
    	if(Utility.isMd5(md5)){
    		assertEquals("a448e2bdbcdc9f8271ab518a9490229c".toUpperCase(),
    					 md5.toUpperCase());
    	} else {
    		assertEquals(md5, "Valid MD5"); //not valid
    	}
    }
    
    public void testPasswordSHA1()
    {
    	String sha256 = DigestUtils.sha256Hex("MandioquinhaNoMcDonalds");
    	if(Utility.isValidSHA256(sha256)){
    		assertEquals("765E2462EEABBDBAD010316AB3DCE7884C415CF02F0E1CA6CF55C929DD712C8D".toUpperCase(),
    					 sha256.toUpperCase());
    	} else {
    		assertEquals(sha256, "Valid SHA256"); //not valid
    	}
    }
}
