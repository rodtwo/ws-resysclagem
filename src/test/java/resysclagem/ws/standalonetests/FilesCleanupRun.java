package resysclagem.ws.standalonetests;

import org.junit.Test;

import junit.framework.TestCase;
import resysclagem.ws.PropertySource;
import resysclagem.ws.config.jobs.FilesCleanup;

public class FilesCleanupRun extends TestCase {

	@Test
	public static void main(String[] args) {
		Boolean prod = Boolean.valueOf(PropertySource.props.getProperty("resysclagem.launch.prod"));
		String sufixoProd = "";
		
		if(prod){
			System.out.println("* Production mode ON (Prod mode)");
			sufixoProd = ".prod";
		} else {
			System.out.println("* Not prod application (Dev mode)");
		}
		
		String dbUrl = PropertySource.props.getProperty("resysclagem.bd"+ sufixoProd +".url");
		String dbUsuario = PropertySource.props.getProperty("resysclagem.bd"+ sufixoProd +".usuario");
        String dbSenha = PropertySource.props.getProperty("resysclagem.bd"+ sufixoProd +".senha"); // pensar em em maneira de encriptar
        
        try {
        	boolean done = FilesCleanup.doFileCleanup(dbUrl, dbUsuario, dbSenha, true);
        	assertTrue(done);
        } catch (Exception e){
        	e.printStackTrace();
        }

	}

}
