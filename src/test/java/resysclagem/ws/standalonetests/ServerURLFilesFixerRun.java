package resysclagem.ws.standalonetests;

import org.junit.Test;

import junit.framework.TestCase;
import resysclagem.ws.PropertySource;
import resysclagem.ws.config.jobs.FilesCleanup;
import resysclagem.ws.config.jobs.ServerURLFilesFixer;

public class ServerURLFilesFixerRun extends TestCase {

	@Test
	public static void main(String[] args) {
		Boolean prod = Boolean.valueOf(PropertySource.props.getProperty("resysclagem.launch.prod"));
		String sufixoProd = "";
		
		if(prod){
			System.out.println("* Production mode ON (Prod mode)");
			sufixoProd = ".prod";
		} else {
			System.out.println("* Not prod application (Dev mode)");
		}
		
		String dbUrl = PropertySource.props.getProperty("resysclagem.bd"+ sufixoProd +".url");
		String dbUsuario = PropertySource.props.getProperty("resysclagem.bd"+ sufixoProd +".usuario");
        String dbSenha = PropertySource.props.getProperty("resysclagem.bd"+ sufixoProd +".senha"); // pensar em em maneira de encriptar
        String protocol = PropertySource.props.getProperty("resysclagem.launch"+ sufixoProd +".protocol");
        
        String currIpAndPort = PropertySource.props.getProperty("resysclagem.launch"+ sufixoProd +".host")
        			   + ":" + PropertySource.props.getProperty("resysclagem.launch"+ sufixoProd +".port");
        String fileDir = PropertySource.props.getProperty("resysclagem.file");	// e.g. /file
        
        try {
        	boolean done = ServerURLFilesFixer.doFileURLFix(dbUrl, dbUsuario, dbSenha, protocol, currIpAndPort, fileDir);
        	assertTrue(done);
        } catch (Exception e){
        	e.printStackTrace();
        	fail();
        }

	}

}
